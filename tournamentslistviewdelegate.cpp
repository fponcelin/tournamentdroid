#include "tournamentslistviewdelegate.h"

int tournamentsListViewDelegate::padding = 5;

tournamentsListViewDelegate::tournamentsListViewDelegate()
{

}

QSize tournamentsListViewDelegate::sizeHint(const QStyleOptionViewItem &  option ,
                                        const QModelIndex & index) const
{
    if(!index.isValid())
        return QSize();

    QString tournamentNameText = index.data(TournamentName).toString();
    QString playersCountText = index.data(PlayersCount).toString() + " participants";

    QFont tournamentNameFont = QApplication::font();
    tournamentNameFont.setBold(true);
    tournamentNameFont.setPointSize(16);
    QFont tournamentDateFont = QApplication::font();
    tournamentDateFont.setBold(true);
    tournamentDateFont.setPointSize(16);
    QFont playersCountFont = QApplication::font();
    QFontMetrics tournamentNameFm(tournamentNameFont);
    QFontMetrics playersCountFm(playersCountFont);

    /* No need for x,y here. we only need to calculate the height given the width.
     * Note that the given height is 0. That is because boundingRect() will return
     * the suitable height if the given geometry does not fit. And this is exactly
     * what we want.
     */
    QRect tournamentNameRect = tournamentNameFm.boundingRect(0, 0, option.rect.width()/2, 0, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, tournamentNameText);
    QRect playersCountRect = playersCountFm.boundingRect(0, 0, option.rect.width()/2, 0, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, playersCountText);

    QSize size(option.rect.width(), tournamentNameRect.height() + playersCountRect.height() +  3*padding);

    return size;
}

void tournamentsListViewDelegate::paint(QPainter *painter,
                                    const QStyleOptionViewItem &option,
                                    const QModelIndex &index) const
{
    if(!index.isValid())
        return;

    QString locale = QLocale::system().name();
    bool isLocaleFR = locale.contains("fr");

    painter->save();

    if (option.state & QStyle::State_Selected)
        painter->fillRect(option.rect, option.palette.highlight());

    QString tournamentNameText = index.data(TournamentName).toString();
    QString tournamentDateText = index.data(TournamentDate).toString();
    QString playersCountText = index.data(PlayersCount).toString() + " participants";
    QString winnerNameText;

    if (isLocaleFR)
        winnerNameText += "Vainqueur : " + index.data(WinnerName).toString();
    else
        winnerNameText += "Winner: " + index.data(WinnerName).toString();

    QFont tournamentNameFont = QApplication::font();
    tournamentNameFont.setBold(true);
    tournamentNameFont.setPointSize(16);
    QFont tournamentDateFont = QApplication::font();
    tournamentDateFont.setBold(true);
    tournamentDateFont.setPointSize(16);
    QFont playersCountFont = QApplication::font();
    QFont winnerNameFont = QApplication::font();
    QFontMetrics tournamentNameFm(tournamentNameFont);
    QFontMetrics tournamentDateFm(tournamentDateFont);
    QFontMetrics playersCountFm(playersCountFont);
    QFontMetrics winnerNameFm(winnerNameFont);

    /*
     * The x,y coords are not (0,0) but values given by 'option'. So, calculate the
     * rects again given the x,y,w.
     * Note that the given height is 0. That is because boundingRect() will return
     * the suitable height if the given geometry does not fit. And this is exactly
     * what we want.
     */

    QRect tournamentNameRect = tournamentNameFm.boundingRect(option.rect.left() + padding, option.rect.top() + padding, option.rect.width()/2, 0, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, tournamentNameText);
    QRect tournamentDateRect = tournamentDateFm.boundingRect(option.rect.right() - option.rect.width()/2, option.rect.top() + padding, option.rect.width()/2, 0, Qt::AlignRight|Qt::AlignTop|Qt::TextWordWrap, tournamentDateText);
    QRect playersCountRect = playersCountFm.boundingRect(option.rect.left() + padding, tournamentNameRect.bottom() + padding, option.rect.width()/2, 0, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, playersCountText);
    QRect winnerNameRect = winnerNameFm.boundingRect(option.rect.left()+ 100 + padding, tournamentDateRect.bottom() + padding, option.rect.width()/2, 0, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, winnerNameText);

    painter->setPen(Qt::green);

    painter->setFont(tournamentNameFont);
    painter->drawText(tournamentNameRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, tournamentNameText);

    painter->setFont(tournamentDateFont);
    painter->drawText(tournamentDateRect, Qt::AlignRight|Qt::AlignTop|Qt::TextWordWrap, tournamentDateText);

    painter->setFont(playersCountFont);
    painter->drawText(playersCountRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, playersCountText);

    painter->setFont(winnerNameFont);
    painter->drawText(winnerNameRect, Qt::AlignRight|Qt::AlignTop|Qt::TextWordWrap, winnerNameText);

    painter->restore();
}
