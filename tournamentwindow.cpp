#include "tournamentwindow.h"
#include "ui_tournamentwindow.h"
#include <QDebug>
#include <QMessageBox>
#include <math.h>

tournamentWindow::tournamentWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::tournamentWindow)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);
    ui->previousRoundPushButton->setEnabled(false);

    playersRankingsListViewModel = new QStandardItemModel();
    tablesListViewModel = new QStandardItemModel();
    currentPlayersListRankingsViewDelegate = new playersRankingsListViewDelegate;
    currentTablesListViewDelegate = new tablesListViewDelegate;

    ui->playersRankingsListView->setItemDelegate(currentPlayersListRankingsViewDelegate);
    ui->tablesListView->setItemDelegate(currentTablesListViewDelegate);
    ui->playersRankingsListView->setModel(playersRankingsListViewModel);
    ui->tablesListView->setModel(tablesListViewModel);

    ui->newGameRoundPushButton->setText("");
    ui->newGameRoundPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_NewTurn_Normal.png);background-color: rgba(0,0,0,0);} QPushButton::hover {image: url(:/img/Btn_NewTurn_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_NewTurn_Pressed.png);}");
    ui->finishTournamentPushButton->setText("");
    ui->finishTournamentPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_EndTournament_Normal.png);background-color: rgba(0,0,0,0);} QPushButton::hover {image: url(:/img/Btn_EndTournament_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_EndTournament_Pressed.png);}");
    ui->previousRoundPushButton->setText("");
    ui->previousRoundPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_GoPreviousTurn_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_GoPreviousTurn_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_GoPreviousTurn_Pressed.png);} QPushButton::disabled {image: url(:/img/Btn_GoPreviousTurn_Disabled.png);}");
    ui->finishTablePushButton->setText("");
    ui->finishTablePushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_EndSelectedTable_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_EndSelectedTable_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_EndSelectedTable_Pressed.png);} QPushButton::disabled {image: url(:/img/Btn_EndSelectedTable_Disabled.png);}");
}

tournamentWindow::~tournamentWindow()
{
    delete ui;
}

void tournamentWindow::setCurrentTournament(tournament *p_currentTournament, bool restoredFromDb) {
    currentTournament = p_currentTournament;
    for (player *p : currentTournament->getPlayersVector()) {
        connect (p, SIGNAL(updatedScores()), this, SLOT(refreshPlayersRankings()));
    }

    int fit = false;
    ui->tournamentNameLabel->setText("");
    QFont myFont = ui->tournamentNameLabel->font();
    while (!fit)
    {
        QFontMetrics fm( myFont );
        QRect bound = fm.boundingRect(0,0, ui->tournamentNameLabel->width(), ui->tournamentNameLabel->height(), Qt::TextWordWrap | Qt::AlignLeft, currentTournament->getName());

        if (bound.width() <= ui->tournamentNameLabel->width() &&
            bound.height() <= ui->tournamentNameLabel->height())
            fit = true;
        else
            myFont.setPointSize(myFont.pointSize() - 1);
    }
    ui->tournamentNameLabel->setFont(myFont);
    ui->tournamentNameLabel->setText(currentTournament->getName());
    ui->previousRoundPushButton->setEnabled(false);
    ui->finishTablePushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_EndSelectedTable_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_EndSelectedTable_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_EndSelectedTable_Pressed.png);} QPushButton::disabled {image: url(:/img/Btn_EndSelectedTable_Disabled.png);}");
    ui->finishTablePushButton->setEnabled(false);

    qDebug() << "about to refreshPlayersRankings";
    refreshPlayersRankings();
    qDebug() << "refreshPlayersRankings OK, about to addRound";
    if (!restoredFromDb)
        currentTournament->addRound();
    else if (currentTournament->getCurrentRoundId() > 0) {
        //ui->finishTablePushButton->setText("Voir tour suivant       >");
        ui->finishTablePushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_GoNextTurn_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_GoNextTurn_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_GoNextTurn_Pressed.png);}");
        ui->finishTablePushButton->setEnabled(true);
    }

    qDebug() << "addRound OK, about to refreshTablesListView";
    for (table *t : currentTournament->getRoundsVector()[currentTournament->getCurrentRoundId()]->getTablesVector()) {
        connect(t, SIGNAL(tableResultsUpdated()), this, SLOT(refreshTablesListView()));
    }
    refreshTablesListView();    
    qDebug() << "refreshTablesListView OK, about to emit receivedTournament";
    emit receivedTournament();
}

void tournamentWindow::on_finishTournamentPushButton_clicked()
{
    if (!currentTournament->getIsRestoredFromDb()) {
        //Save tournament to database and go back to welcomeWindow
        QMessageBox msgBox;
        msgBox.setText(tr("Vous allez quitter ce tournoi."));
        msgBox.setInformativeText(tr("Voulez-vous l'enregistrer ?"));
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        msgBox.setButtonText(0, tr("Enregistrer"));
        //msgBox.button(QMessageBox::Save)->setStyleSheet("");
        int userChoice = msgBox.exec();
        QSqlQuery q(QSqlDatabase::database("dbConnection"));

        switch (userChoice) {
        case QMessageBox::Save:
            //Delete tournament pointer and go back to welcomeWindow
            quitTournament();
            break;

        case QMessageBox::Discard:
            //Delete tournament pointer, delete tournament and related data from database and go back to welcomeWindow
            if (!q.prepare("DELETE FROM tournaments WHERE tournamentId=" + QString::number(currentTournament->getTournamentDbId()))) {
                    qDebug() << q.lastError();
            }
            q.exec();

            quitTournament();
            break;

        case QMessageBox::Cancel:
            //Go back to the tournament
            break;

        default:
            //The app will crash, we're doomed!!!
            break;
        }
    } else {
        quitTournament();
    }
}

void tournamentWindow::on_newGameRoundPushButton_clicked()
{
    bool gameRoundisOver = true;
    for (table *t : currentTournament->getRoundsVector()[currentTournament->getCurrentRoundId()]->getTablesVector()) {
        if (!t->getIsTableRoundOver()) {
            gameRoundisOver = false;
        }
    }
    if (!gameRoundisOver) {
        warnUserTableRoundIsNotOver();
    } else {
        if (currentTournament->getCurrentRoundId() < currentTournament->getRoundNb() - 1) {
            currentTournament->addRound();
            for (table *t : currentTournament->getRoundsVector()[currentTournament->getCurrentRoundId()]->getTablesVector()) {
                connect(t, SIGNAL(tableResultsUpdated()), this, SLOT(refreshTablesListView()));
            }
            refreshTablesListView();
            ui->previousRoundPushButton->setEnabled(true);
            ui->finishTablePushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_EndSelectedTable_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_EndSelectedTable_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_EndSelectedTable_Pressed.png);} QPushButton::disabled {image: url(:/img/Btn_EndSelectedTable_Disabled.png);}");
            ui->finishTablePushButton->setEnabled(false);
        } else {
            if (currentTournament->getHasTopCut() && currentTournament->getCurrentRoundId() < currentTournament->getRoundNb() + currentTournament->getTopCutNb() - 1) {
                currentTournament->addEliminationRound();
                for (table *t : currentTournament->getRoundsVector()[currentTournament->getCurrentRoundId()]->getTablesVector()) {
                    connect(t, SIGNAL(tableResultsUpdated()), this, SLOT(refreshTablesListView()));
                }
                refreshTablesListView();
                ui->previousRoundPushButton->setEnabled(true);
                ui->finishTablePushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_EndSelectedTable_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_EndSelectedTable_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_EndSelectedTable_Pressed.png);} QPushButton::disabled {image: url(:/img/Btn_EndSelectedTable_Disabled.png);}");
                ui->finishTablePushButton->setEnabled(false);
            } else {
                warnUserTournamentIsOver();
            }

        }
    }
}

void tournamentWindow::refreshPlayersRankings()
{
    currentTournament->sortPlayersVectorDescending();
    playersRankingsListViewModel->clear();
    int playerIndex = 1;

    //loop to fill the playersRankingsListView
    for (player *p : currentTournament->getPlayersVector()) {
        QStandardItem *item = new QStandardItem();
        item->setData(p->getName(), playersRankingsListViewDelegate::PlayerName);
        item->setData(p->getFactionId(), playersRankingsListViewDelegate::PlayerFaction);
        item->setData(p->getTournamentPoints(), playersRankingsListViewDelegate::PlayerTournamentPts);
        item->setData(p->getMarginOfVictory(), playersRankingsListViewDelegate::PlayerMoV);
        item->setData(playerIndex, playersRankingsListViewDelegate::PlayerIndex);
        if (p->getTopCutRank() > 0 && p->getTopCutRank() < 10) {
            item->setData("Top " + QString::number(qPow(2, currentTournament->getTopCutNb() - (p->getTopCutRank() - 1))), playersRankingsListViewDelegate::TopCutBracket);
        } else if (currentTournament->getHasTopCut() && currentTournament->getCurrentRoundId() == currentTournament->getRoundNb() + currentTournament->getTopCutNb() - 1 && p->getTopCutRank() == 32) {
            item->setData(tr("Vainqueur"), playersRankingsListViewDelegate::TopCutBracket);
        }
        playersRankingsListViewModel->appendRow(item);
        playerIndex++;
    }
}

void tournamentWindow::refreshTablesListView() {
    bool gameRoundisOver = true;
    tablesListViewModel->clear();
    if (currentTournament->getDisplayedRoundId() < currentTournament->getRoundNb()) {
        ui->gameRoundNbLabel->setText(tr("Ronde suisse Nº") + QString::number(currentTournament->getDisplayedRoundId() + 1));
    } else {
        ui->gameRoundNbLabel->setText("Top " + QString::number(qPow(2, currentTournament->getTopCutNb() - (currentTournament->getDisplayedRoundId() - currentTournament->getRoundNb()))));
    }

    qDebug() << "Cleared tablesListView, going into loop to fill";

    //Loop to fill the tablesListView
    for (table *t : currentTournament->getRoundsVector()[currentTournament->getDisplayedRoundId()]->getTablesVector()) {
        qDebug() << "Looping to fill tablesListView";
        qDebug() << "Filling table:";
        qDebug() << t->getTableId();
        QString tableTitle = QString::number(t->getTableId() + 1);
        if (t->getIsTableRoundOver())
            tableTitle += " - OK";
        else
            gameRoundisOver = false;

        QStandardItem *item = new QStandardItem();
        item->setData(tableTitle, tablesListViewDelegate::TableId);
        item->setData(t->getPlayer0()->getName(), tablesListViewDelegate::Player0Name);
        item->setData(t->getPlayer0Score(), tablesListViewDelegate::Player0PtsDestroyed);
        item->setData(t->getPlayer1()->getName(), tablesListViewDelegate::Player1Name);
        item->setData(t->getPlayer1Score(), tablesListViewDelegate::Player1PtsDestroyed);
        tablesListViewModel->appendRow(item);
    }
    if (((currentTournament->getHasTopCut() && currentTournament->getCurrentRoundId() == currentTournament->getRoundNb() + currentTournament->getTopCutNb() - 1) || (!currentTournament->getHasTopCut() && currentTournament->getCurrentRoundId() == currentTournament->getRoundNb() - 1)) && gameRoundisOver) {
        currentTournament->endTournament();
    }
}

void tournamentWindow::warnUserTableRoundIsNotOver() {
    QMessageBox msgBox;
    msgBox.setText(tr("La ronde en cours n'est pas terminée.\nVeuillez entrer les résultats pour toutes les tables."));
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.exec();
}

void tournamentWindow::warnUserTournamentIsOver() {
    QMessageBox msgBox;
    msgBox.setText(tr("Le tournoi a atteint son dernier tour, vous ne pouvez pas commencer de nouvelle ronde."));
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.exec();
}

void tournamentWindow::saveTableToDb(table *updatedTable) {
    QSqlQuery q(QSqlDatabase::database("dbConnection"));
    if (!q.prepare("UPDATE tables SET tablePlayer0Score=" + QString::number(updatedTable->getPlayer0Score()) + ", tablePlayer1Score=" + QString::number(updatedTable->getPlayer1Score()) + ", tableRoundIsOver=1 WHERE tableId=" + QString::number(updatedTable->getTableDbId()))) {
        qDebug() << q.lastError();
    }
    q.exec();
}

void tournamentWindow::on_finishTablePushButton_clicked()
{
    if (currentTournament->getDisplayedRoundId() == currentTournament->getCurrentRoundId()) {
        openTableResultsInputDialog();
    } else {
        currentTournament->setDisplayedRoundId(currentTournament->getDisplayedRoundId() + 1);
        ui->previousRoundPushButton->setEnabled(true);
        if (currentTournament->getDisplayedRoundId() == currentTournament->getCurrentRoundId()) {
            ui->finishTablePushButton->setEnabled(false);
            //ui->finishTablePushButton->setText("Terminer table sélectionnée");
            ui->finishTablePushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_EndSelectedTable_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_EndSelectedTable_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_EndSelectedTable_Pressed.png);} QPushButton::disabled {image: url(:/img/Btn_EndSelectedTable_Disabled.png);}");
        }
        refreshTablesListView();
    }

}

void tournamentWindow::on_tablesListView_clicked(const QModelIndex &index)
{
    if (index.isValid()) {
        ui->finishTablePushButton->setEnabled(true);
    }
}

void tournamentWindow::on_previousRoundPushButton_clicked()
{
    if (currentTournament->getDisplayedRoundId() > 0) {
        currentTournament->setDisplayedRoundId(currentTournament->getDisplayedRoundId() - 1);
        ui->finishTablePushButton->setEnabled(true);
        //ui->finishTablePushButton->setText("Voir tour suivant       >");
        ui->finishTablePushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_GoNextTurn_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_GoNextTurn_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_GoNextTurn_Pressed.png);}");
        if (currentTournament->getDisplayedRoundId() == 0) {
            ui->previousRoundPushButton->setEnabled(false);
        }
        refreshTablesListView();
    }
}

void tournamentWindow::quitTournament() {
    delete currentTournament;
    emit tournamentEnded();
}

void tournamentWindow::openTableResultsInputDialog() {
    int selectedRow = ui->tablesListView->currentIndex().row();
    if (currentTournament->getCurrentRoundId() < currentTournament->getRoundNb()) {
        resultsInputDialog = new tableResultsInputDialog(currentTournament->getRoundsVector()[currentTournament->getCurrentRoundId()]->getTablesVector()[selectedRow]);
    } else {
        resultsInputDialog = new tableResultsInputDialog(currentTournament->getRoundsVector()[currentTournament->getCurrentRoundId()]->getTablesVector()[selectedRow], currentTournament->getCurrentRoundId() - currentTournament->getRoundNb());
    }
    //connect(resultsInputDialog, SIGNAL(tableUpdated(table*)), this, SLOT(saveTableToDb(table*)));
    resultsInputDialog->show();
    ui->finishTablePushButton->setEnabled(false);
}

void tournamentWindow::on_tablesListView_doubleClicked(const QModelIndex &index)
{
    if (index.isValid() && currentTournament->getDisplayedRoundId() == currentTournament->getCurrentRoundId())
        openTableResultsInputDialog();
}
