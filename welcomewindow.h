#ifndef WELCOMEWINDOW_H
#define WELCOMEWINDOW_H

#include <QWidget>
#include <QAbstractItemView>
#include <QStandardItemModel>
#include "playerinputwindow.h"
#include "tournamentwindow.h"
#include "dbmanager.h"
#include "tournamentslistviewdelegate.h"

namespace Ui {
class welcomeWindow;
}

/*!
 * \brief The welcomeWindow class
 *
 * This class manages the user interface for the starting screen of the application.
 * It will display the list of previous tournaments stored in the database and provide fields to create a new tournament.
 */
class welcomeWindow : public QWidget
{
    Q_OBJECT

public:
    /*!
     * \brief welcomeWindow
     *
     * Class constructor
     *
     * \param parent
     */
    explicit welcomeWindow(QWidget *parent = 0);
    ~welcomeWindow();

    /*!
     * \brief setCurrentTournamentWindow
     *
     * This method assigns a tournamentWindow pointer to the currentTournamentWindow member.
     *
     * \param p_currentTournamentWindow
     */
    void setCurrentTournamentWindow(tournamentWindow *p_currentTournamentWindow);

public slots:
    /*!
     * \brief playerInputWindow_closed
     *
     * Slot receiving the destruction signal emitted by a playerInputWindow when closed.
     *
     * This method will enable the welcomeWindow widget and set the focus on the nameLineEdit field.
     */
    void playerInputWindow_closed();

    /*!
     * \brief refreshTournamentsListView
     *
     * This method clears the tournamentsListView and the tournamentDbIdVector of all values.
     * It retrieves all tournament records from the database, then assigns the appropriate data to the tournamentsListViewModel to display in the tournamentsListView.
     * It also adds the record Primary Keys to the tournamentDbIdVector.
     */
    void refreshTournamentsListView();

private slots:
    /*!
     * \brief on_createTournamentPushButton_clicked
     *
     * Slot receiving the createTournamentPushButton click signal.
     *
     * This method creates a new playerInputWindow, passing on a new tournament instance created using information input in the fields.
     * It then disables the content of the widget to prevent input while the playerInputWindow is open.
     */
    void on_createTournamentPushButton_clicked();

    /*!
     * \brief on_deleteTournamentPushButton_clicked
     *
     * Slot receiving the deleteTournamentPushButton click signal.
     *
     * This method retrieves the selected row of the tournamentsListView.
     * It creates a QMessageBox asking the player to confirm the deletion of the database record.
     * If yes, it will use the row's index to retrieve the tournament record's Primary Key from the tournamentDbIdVector and then delete the record from the database.
     * It will then call the refreshTournamentsListView method to update the displayed the listView.
     */
    void on_deleteTournamentPushButton_clicked();

    /*!
     * \brief on_tournamentsListView_clicked
     *
     * Slot receiving the tournamentsListView click signal when a row is selected.
     *
     * This method will enable the deleteTournamentPushButton to allow deletion of a tournament from the tournamentsListView.
     *
     * \param index: the selected row's index
     */
    void on_tournamentsListView_clicked(const QModelIndex &index);

    void on_topCutCheckBox_stateChanged(int arg1);

    void on_openTournamentPushButton_clicked();

    void on_tournamentsListView_doubleClicked(const QModelIndex &index);

private:
    void openTournamentFromDb();

    Ui::welcomeWindow *ui; /*!< User interface object*/
    playerInputWindow *inputWindow; /*!< A playerInputWindow*/
    tournamentWindow *currentTournamentWindow; /*!< The tournamentWindow widget, passed on to the playerInputWindow*/
    QStandardItemModel *tournamentsListViewModel; /*!< List model to display the tournaments data*/
    tournamentsListViewDelegate *myTournamentsListRankingsViewDelegate; /*!< Delegate managing the tournamentsListView line formatting*/
    QVector<QString> tournamentDbIdVector; /*!< Vector of database Primary Keys*/
    QStringListModel *comboBoxModel; /*!< List model to display the top cut options in the comboBox*/
};

#endif // WELCOMEWINDOW_H
