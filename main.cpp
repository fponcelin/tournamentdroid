/**
  * \file main.cpp
  * \brief TournamentDroid - an X-Wing Miniatures Tournament management application
  * \author Florian Poncelin
  * \version 1.0
  * \date 06/04/2016
  *
  * This program is designed to help X-Wing Miniatures Tournament organisers in the management and running of their tournaments.
  */


#include "mainwindow.h"
#include "dbmanager.h"
#include <QApplication>
#include <QTime>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString locale = QLocale::system().name();
    bool isLocaleFR = locale.contains("fr");

    QTranslator translator;
    if (translator.load("tournamentdroid_en", ":/translations") && !isLocaleFR) {
        a.installTranslator(&translator);
    }

    QString dbPath = QDir::homePath() + "/Application Data/TournamentDroid/";
    qDebug() << dbPath;
    if (!QDir(dbPath).exists()) {
        QDir().mkpath(dbPath);
    }

    QSettings settings("Florian Poncelin", "TournamentDroid");
    settings.setValue("databasePath", dbPath + "TournamentDroid.db");
    settings.setValue("userGuidePath", dbPath + "TournamentDroid V1.0 - User guide EN.pdf");
    settings.setValue("webUserGuidePath", QDir::rootPath() + "Program Files/TournamentDroid/webUserguide/index.html");
    settings.setValue("webUserGuidePathWin10", QDir::rootPath() + "Program Files (x86)/TournamentDroid/webUserguide/index.html");
    QFile userGuide(":/doc/TournamentDroid V1.0 - User guide EN.pdf");
    userGuide.copy(settings.value("userGuidePath").toString());

    //Seed for random number generator
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    MainWindow w;
    w.show();

    return a.exec();
}
