#ifndef MANAGECLUBSWINDOW_H
#define MANAGECLUBSWINDOW_H

#include <QWidget>
#include <QListWidgetItem>
#include "dbmanager.h"

namespace Ui {
class manageClubsWindow;
}

/*!
 * \brief The manageClubsWindow class
 *
 * This class manages the clubs management window.
 */
class manageClubsWindow : public QWidget
{
    Q_OBJECT

public:
    explicit manageClubsWindow(QWidget *parent = 0);
    ~manageClubsWindow();

private slots:
    /*!
     * \brief on_closePushButton_clicked
     *
     * Slot receiving the click signal from the closePushButton.
     * This method closes the window.
     */
    void on_closePushButton_clicked();

    /*!
     * \brief on_clubsListWidget_itemClicked
     *
     * Slot receiving the click signal from the clubsListWidget when an item is selected.
     * This method checks that the index selected is valid, fills the clubNameLineEdit with the selected club name
     * and enables modifyClubPushButton and DeleteClubPushButton.
     *
     * \param item: the selected listWidgetItem index
     */
    void on_clubsListWidget_itemClicked(QListWidgetItem *item);

    /*!
     * \brief on_modifyClubPushButton_clicked
     *
     * Slot receiving the click signal from modifyClubPushButton.
     * This method checks that the clubNameLineEdit is not empty and updates the clubs database record with the new club name.
     */
    void on_modifyClubPushButton_clicked();

    /*!
     * \brief on_deleteClubPushButton_clicked
     *
     * Slot receiving the click signal from deletePushButton.
     * This method deletes the selected club from the database.
     */
    void on_deleteClubPushButton_clicked();

    void on_addClubPushButton_clicked();

private:
    Ui::manageClubsWindow *ui; /*!< User interface object*/
    int selectedClubDbId; /*!< Selected club's database Primary Key*/

    /*!
     * \brief refreshClubsList
     *
     * Method clearing the clubListWidget and filling it with clubs from the database.
     */
    void refreshClubsList();

    /*!
     * \brief resetButtons
     *
     * Method disabling the buttons and clearing the clubNameLineEdit.
     */
    void resetButtons();

    void drawBackgroundImg();
};

#endif // MANAGECLUBSWINDOW_H
