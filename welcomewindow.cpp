#include "welcomewindow.h"
#include "ui_welcomewindow.h"
#include "tournament.h"
#include <QDebug>
#include <QMessageBox>

welcomeWindow::welcomeWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::welcomeWindow)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    tournamentsListViewModel = new QStandardItemModel;
    myTournamentsListRankingsViewDelegate = new tournamentsListViewDelegate;

    comboBoxModel = new QStringListModel(this);
    QStringList topCutList;
    topCutList << "Top 4" << "Top 8" << "Top 16" << "Top 32";
    comboBoxModel->setStringList(topCutList);
    ui->topCutComboBox->setModel(comboBoxModel);

    ui->createTournamentPushButton->setText("");
    ui->createTournamentPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_Create_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_Create_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_Create_Pressed.png);}");
    ui->deleteTournamentPushButton->setText("");
    ui->deleteTournamentPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_DeleteSelectedTournament_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_DeleteSelectedTournament_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_DeleteSelectedTournament_Pressed.png);} QPushButton::disabled {image: url(:/img/Btn_DeleteSelectedTournament_Disabled.png);}");
    ui->openTournamentPushButton->setText("");
    ui->openTournamentPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_OpenSelectedTournament_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_OpenSelectedTournament_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_OpenSelectedTournament_Pressed.png);} QPushButton::disabled {image: url(:/img/Btn_OpenSelectedTournament_Disabled.png);}");

    ui->tournamentsListView->setItemDelegate(myTournamentsListRankingsViewDelegate);
    ui->tournamentsListView->setModel(tournamentsListViewModel);
    ui->tournamentsListView->setStyleSheet("background-color: rgba(0,0,0,1);");
    refreshTournamentsListView();
    ui->dateEdit->setDate(QDate::currentDate());
    ui->nameLineEdit->setFocus();
}

welcomeWindow::~welcomeWindow()
{
    delete ui;
}

void welcomeWindow::setCurrentTournamentWindow(tournamentWindow *p_currentTournamentWindow) {
    currentTournamentWindow = p_currentTournamentWindow;
}

void welcomeWindow::on_createTournamentPushButton_clicked()
{
    if (ui->nameLineEdit->text() != "") {
        inputWindow = new playerInputWindow();
        connect(inputWindow, SIGNAL(destroyed(QObject*)), this, SLOT(playerInputWindow_closed()));
        tournament *newTournament = new tournament(ui->nameLineEdit->text(), ui->dateEdit->date(), ui->roundNbspinBox->value(), ui->topCutCheckBox->isChecked());
        if (ui->topCutCheckBox->isChecked()) {
            newTournament->setTopCutNb(ui->topCutComboBox->currentIndex() + 2);
        }
        inputWindow->setCurrentTournament(newTournament);
        inputWindow->setCurrentTournamentWindow(currentTournamentWindow);
        inputWindow->show();
        ui->nameLineEdit->clear();
        this->setDisabled(true);
    }
}

void welcomeWindow::playerInputWindow_closed() {
    this->setEnabled(true);
    ui->nameLineEdit->setFocus();
}

void welcomeWindow::refreshTournamentsListView() {
    tournamentsListViewModel->clear();
    tournamentDbIdVector.clear();
    QSettings settings("Florian Poncelin", "TournamentDroid");
    dbManager db(settings.value("databasePath").toString());

    QSqlQuery q(QLatin1String("SELECT * FROM tournaments"), QSqlDatabase::database("dbConnection"));
    qDebug() << q.isValid();

    while (q.next()) {
        QStandardItem *item = new QStandardItem();
        item->setData(q.value("tournamentName").toString(), tournamentsListViewDelegate::TournamentName);
        qDebug() << q.value("tournamentName").toString();
        item->setData(q.value("tournamentDate").toDate().toString("dd/MM/yyyy"), tournamentsListViewDelegate::TournamentDate);

        QSqlQuery qPlayersNb("SELECT COUNT(*) FROM players WHERE fk_tournamentId = " + q.value(0).toString(), QSqlDatabase::database("dbConnection"));
        if (qPlayersNb.first()) {
            qDebug() << qPlayersNb.value(0).toString();
            item->setData(qPlayersNb.value(0).toString(), tournamentsListViewDelegate::PlayersCount);
        }

        QSqlQuery qPlayers("SELECT playerName FROM players WHERE fk_tournamentId = " + q.value(0).toString() + " ORDER BY playerTopCutRank DESC, playerTournamentPoints DESC, playerMarginOfVictory DESC", QSqlDatabase::database("dbConnection"));
        if (qPlayers.first()) {
            item->setData(qPlayers.value("playerName").toString(), tournamentsListViewDelegate::WinnerName);
            qDebug() << qPlayers.value("playerName").toString();
        }
        tournamentsListViewModel->appendRow(item);
        tournamentDbIdVector.append(q.value(0).toString());
    }
}

void welcomeWindow::on_deleteTournamentPushButton_clicked()
{
    ui->deleteTournamentPushButton->setEnabled(false);
    int tournamentsListIndex = ui->tournamentsListView->currentIndex().row();

    QMessageBox msgBox;
    msgBox.setText(tr("Vous allez supprimer ce tournoi."));
    msgBox.setInformativeText(tr("Etes-vous sûr ?"));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Yes);
    int userChoice = msgBox.exec();
    QSqlQuery q(QSqlDatabase::database("dbConnection"));

    switch (userChoice) {
    case QMessageBox::Yes:
        if (!q.prepare("DELETE FROM tournaments WHERE tournamentId=" + tournamentDbIdVector[tournamentsListIndex])) {
                qDebug() << q.lastError();
        }
        q.exec();
        refreshTournamentsListView();
        break;

    case QMessageBox::Cancel:
        ui->tournamentsListView->clearSelection();
        break;

    default:
        //The app will crash, we're doomed!!!
        break;
    }
}

void welcomeWindow::on_tournamentsListView_clicked(const QModelIndex &index)
{
    if (index.isValid()) {
        ui->deleteTournamentPushButton->setEnabled(true);
        ui->openTournamentPushButton->setEnabled(true);
    }
}

void welcomeWindow::on_topCutCheckBox_stateChanged(int arg1)
{
    if (arg1) {
        ui->topCutComboBox->setEnabled(true);
    } else {
        ui->topCutComboBox->setEnabled(false);
    }
}

void welcomeWindow::on_openTournamentPushButton_clicked()
{
    openTournamentFromDb();
}

void welcomeWindow::on_tournamentsListView_doubleClicked(const QModelIndex &index)
{
    if (index.isValid())
        openTournamentFromDb();
}

void welcomeWindow::openTournamentFromDb() {
    ui->openTournamentPushButton->setEnabled(false);
    int tournamentsListIndex = ui->tournamentsListView->currentIndex().row();
    QSqlQuery q(QSqlDatabase::database("dbConnection"));
    if (!q.prepare("SELECT * FROM tournaments WHERE tournamentId=" + tournamentDbIdVector[tournamentsListIndex])) {
            qDebug() << q.lastError();
    }
    q.exec();
    if (q.first()) {
        tournament *openedTournament = new tournament(q.value("tournamentName").toString(), q.value("tournamentDate").toDate(), q.value("tournamentTotalRoundNb").toInt(), q.value("tournamentHasTopCut").toBool());
        openedTournament->setTournamentDbId(tournamentDbIdVector[tournamentsListIndex].toInt());
        openedTournament->restoringFromDb();
        if (q.value("tournamentIsOver").toInt() == 1)
            openedTournament->endTournament(true);
        if (q.value("tournamentTopCutNb").toInt() > 0)
            openedTournament->setTopCutNb(q.value("tournamentTopCutNb").toInt());
        //get players from db
        if (!q.prepare("SELECT * FROM players WHERE fk_tournamentId=" + tournamentDbIdVector[tournamentsListIndex])) {
                qDebug() << q.lastError();
        }
        q.exec();

        int vectorIndex = 0;
        while (q.next()) {
            openedTournament->addPlayer(new player(q.value("playerName").toString(), q.value("playerFactionId").toInt(), q.value("playerSquadronPoints").toInt(), false, q.value("fk_clubId").toInt()));
            openedTournament->getPlayersVector()[vectorIndex]->setPlayerDbId(q.value("playerId").toInt());
            openedTournament->getPlayersVector()[vectorIndex]->addScores(q.value("playerTournamentPoints").toInt(), q.value("playerMarginOfVictory").toInt());
            openedTournament->getPlayersVector()[vectorIndex]->setTopCutRank(q.value("playerTopCutRank").toInt());
            vectorIndex++;
        }

        //get gameRounds from db
        if (!q.prepare("SELECT * FROM gamerounds WHERE fk_tournamentId=" + tournamentDbIdVector[tournamentsListIndex] + " ORDER BY gameRoundNb")) {
                qDebug() << q.lastError();
        }
        q.exec();
        vectorIndex = 0;
        while (q.next()) {
            bool gameRoundIsElimination = false;
            if (q.value("gameRoundNb").toInt() >= openedTournament->getRoundNb())
                   gameRoundIsElimination = true;

            QVector<player*> playersVector = openedTournament->getPlayersVector();
            openedTournament->addDbRestoredRound(new gameRound(playersVector, q.value("gameRoundNb").toInt(), gameRoundIsElimination, true));

            //get tables for gameRound
            QSqlQuery qTables(QSqlDatabase::database("dbConnection"));
            if (!qTables.prepare("SELECT * FROM tables WHERE fk_gameRoundId=" + QString::number(q.value("gameRoundId").toInt()) + " ORDER BY tableNb"))
                    qDebug() << qTables.lastError();
            qTables.exec();

            int tablesVectorIndex = 0;
            while (qTables.next()) {
                player *tablePlayer0;
                player *tablePlayer1;
                for (player *p : openedTournament->getPlayersVector()) {
                    if (p->getPlayerDbId() == qTables.value("fk_tablePlayer0Id").toInt())
                        tablePlayer0 = p;
                    if (p->getPlayerDbId() == qTables.value("fk_tablePlayer1Id").toInt())
                        tablePlayer1 = p;
                }
                tablePlayer0->addPreviousOpponentId(tablePlayer1->getPlayerDbId());
                tablePlayer1->addPreviousOpponentId(tablePlayer0->getPlayerDbId());
                openedTournament->getRoundsVector()[vectorIndex]->addDbRestoredTable(new table(tablePlayer0, tablePlayer1, qTables.value("tableNb").toInt()));
                if (qTables.value("tableRoundIsOver") == 1) {
                    openedTournament->getRoundsVector()[vectorIndex]->getTablesVector()[tablesVectorIndex]->setPlayerScores(qTables.value("tablePlayer0Score").toInt(), qTables.value("tablePlayer1Score").toInt());
                }
                tablesVectorIndex++;
            }
            openedTournament->setCurrentRoundId(vectorIndex);
            vectorIndex++;
        }
        currentTournamentWindow->setCurrentTournament(openedTournament, true);
    }
}
