#include "playerinputwindow.h"
#include "ui_playerinputwindow.h"
#include <QPalette>
#include <QDebug>
#include <QInputDialog>

playerInputWindow::playerInputWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::playerInputWindow)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    drawBackgroundImg();
    ui->addClubPushButton->setText("");
    ui->addClubPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_NewClub_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_NewClub_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_NewClub_Pressed.png);}");
    ui->addPlayerPushButton->setText("");
    ui->addPlayerPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_AddPlayer_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_AddPlayer_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_AddPlayer_Pressed.png);}");
    ui->deletePlayerPushButton->setText("");
    ui->deletePlayerPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_DeleteSelectedPlayer_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_DeleteSelectedPlayer_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_DeleteSelectedPlayer_Pressed.png);} QPushButton::disabled {image: url(:/img/Btn_DeleteSelectedPlayer_Disabled.png);}");
    ui->startTournamentPushButton->setText("");
    ui->startTournamentPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_StartTournament_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_StartTournament_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_StartTournament_Pressed.png);}");

    comboBoxModel = new QStringListModel(this);
    QStringList factionList;
    factionList << tr("Rebelles") << tr("Empire") << tr("Racailles & Scélérats");
    comboBoxModel->setStringList(factionList);
    ui->factionComboBox->setModel(comboBoxModel);
    refreshClubComboBox();

    playersListViewModel = new QStandardItemModel();
    currentPlayersListViewDelegate = new playersListViewDelegate();

    ui->playersListView->setItemDelegate(currentPlayersListViewDelegate);
    ui->playersListView->setModel(playersListViewModel);
    ui->nameLineEdit->setFocus();
}

playerInputWindow::~playerInputWindow()
{
    delete ui;
}

void playerInputWindow::resizeEvent(QResizeEvent *evt) {
    drawBackgroundImg();
    QWidget::resizeEvent(evt);
}

void playerInputWindow::drawBackgroundImg() {
    QImage BgTile(":/img/Bg_PlayerInput_Fill.png");
    QImage BgTopImg(":/img/Bg_PlayerInput_Top.png");
    QImage BgBottomImg(":/img/Bg_PlayerInput_Bottom.png");

    QPixmap bgImg(this->size());
    QPainter paint(&bgImg);
    for (int i=0; i<this->height(); i+=50) {
        paint.drawImage(this->rect().left(),i,BgTile);
    }
    paint.drawImage(this->rect().left(),this->rect().top(),BgTopImg);
    paint.drawImage(this->rect().left(),this->rect().bottom() - 49,BgBottomImg);

    QPalette palette = this->palette();
    palette.setBrush(QPalette::Background, bgImg);//set the pic to the background
    this->setPalette(palette); //show the background pic
}

void playerInputWindow::setCurrentTournament(tournament *p_currentTournament) {
    if (currentTournament != p_currentTournament) {
        currentTournament = p_currentTournament;
    }
    this->setWindowTitle("Tournoi " + currentTournament->getName());
    qDebug() << currentTournament->getTopCutNb();
}

void playerInputWindow::setCurrentTournamentWindow(tournamentWindow *p_currentTournamentWindow) {
    currentTournamentWindow = p_currentTournamentWindow;
}

void playerInputWindow::refreshClubComboBox() {
    ui->clubComboBox->clear();
    QSqlQuery q(QLatin1String("SELECT * FROM clubs"), QSqlDatabase::database("dbConnection"));
    while (q.next()) {
        ui->clubComboBox->addItem(q.value("clubName").toString(), q.value("clubId"));
    }
}

void playerInputWindow::on_startTournamentPushButton_clicked()
{
    if (currentTournament->getPlayersVector().count() >= 4) {
        //Save tournament and players to database
        QSettings settings("Florian Poncelin", "TournamentDroid");
        dbManager db(settings.value("databasePath").toString());
        currentTournament->setTournamentDbId(db.saveTournamentToDb(currentTournament));
        qDebug() << currentTournament->getTournamentDbId();

        //Send tournament instance to tournamentWindow and close window
        currentTournamentWindow->setCurrentTournament(currentTournament);
        this->close();
    }
}

void playerInputWindow::on_addPlayerPushButton_clicked()
{
    if (ui->nameLineEdit->text() != "") {
        if (ui->addPlayerPushButton->text() != "  ") {
            currentTournament->addPlayer(new player(ui->nameLineEdit->text(), ui->factionComboBox->currentIndex(), ui->squadPtsSpinBox->value(), ui->hasByeCheckBox->isChecked(), ui->clubComboBox->itemData(ui->clubComboBox->currentIndex()).toInt()));
            QStandardItem *item = new QStandardItem();
            item->setData(currentTournament->getPlayersVector().last()->getName(), playersListViewDelegate::PlayerName);
            item->setData(currentTournament->getPlayersVector().last()->getFactionId(), playersListViewDelegate::PlayerFaction);
            item->setData(currentTournament->getPlayersVector().last()->getSquadronPoints(), playersListViewDelegate::PlayerSquadPts);
            item->setData(ui->clubComboBox->currentText(), playersListViewDelegate::PlayerClub);
            if (ui->hasByeCheckBox->isChecked()) {
                item->setData(" - Bye", playersListViewDelegate::PlayerBye);
            }
            playersListViewModel->appendRow(item);
        } else if (ui->playersListView->currentIndex().isValid()) {
            int selectedRow = ui->playersListView->currentIndex().row();
            player *selectedPlayer = currentTournament->getPlayersVector()[selectedRow];
            selectedPlayer->setName(ui->nameLineEdit->text());
            selectedPlayer->setFactionId(ui->factionComboBox->currentIndex());
            selectedPlayer->setSquadronPoints(ui->squadPtsSpinBox->value());
            selectedPlayer->setHasBye(ui->hasByeCheckBox->isChecked());
            selectedPlayer->setClubDbId(ui->clubComboBox->itemData(ui->clubComboBox->currentIndex()).toInt());

            QStandardItem *item = playersListViewModel->item(selectedRow);
            item->setData(selectedPlayer->getName(), playersListViewDelegate::PlayerName);
            item->setData(selectedPlayer->getFactionId(), playersListViewDelegate::PlayerFaction);
            item->setData(selectedPlayer->getSquadronPoints(), playersListViewDelegate::PlayerSquadPts);
            item->setData(ui->clubComboBox->currentText(), playersListViewDelegate::PlayerClub);
            if (selectedPlayer->getHasBye()) {
                item->setData(" - Bye", playersListViewDelegate::PlayerBye);
            } else {
                item->setData("", playersListViewDelegate::PlayerBye);
            }
            ui->addPlayerPushButton->setText("");
            ui->addPlayerPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_AddPlayer_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_AddPlayer_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_AddPlayer_Pressed.png);}");
            ui->playersListView->clearSelection();
        }

        //Cleaning up the fields for the next input
        ui->nameLineEdit->clear();
        ui->factionComboBox->setCurrentIndex(0);
        ui->clubComboBox->setCurrentIndex(0);
        ui->squadPtsSpinBox->setValue(100);
        ui->hasByeCheckBox->setChecked(false);
        QPalette *blackPalette = new QPalette();
        blackPalette->setColor(QPalette::WindowText,QColor(0, 180, 255));
        ui->nameLabel->setPalette(*blackPalette);
        delete blackPalette;
    } else {
        QPalette *redPalette = new QPalette();
        redPalette->setColor(QPalette::WindowText,Qt::red);
        ui->nameLabel->setPalette(*redPalette);
        delete redPalette;
    }
    ui->nameLineEdit->setFocus();
}

void playerInputWindow::on_deletePlayerPushButton_clicked()
{
    int selectedRow = ui->playersListView->currentIndex().row();
    currentTournament->deletePlayer(selectedRow);
    playersListViewModel->removeRow(selectedRow);
    ui->nameLineEdit->clear();
    ui->factionComboBox->setCurrentIndex(0);
    ui->clubComboBox->setCurrentIndex(0);
    ui->squadPtsSpinBox->setValue(100);
    ui->hasByeCheckBox->setChecked(false);
    ui->nameLineEdit->setFocus();
    ui->deletePlayerPushButton->setEnabled(false);
    ui->addPlayerPushButton->setText("");
    ui->addPlayerPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_AddPlayer_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_AddPlayer_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_AddPlayer_Pressed.png);}");
    ui->playersListView->clearSelection();
}

void playerInputWindow::on_playersListView_clicked(const QModelIndex &index)
{
    if (index.isValid()) {
        ui->deletePlayerPushButton->setEnabled(true);
        int selectedRow = ui->playersListView->currentIndex().row();
        player* selectedPlayer = currentTournament->getPlayersVector()[selectedRow];

        //Fill up fields to allow modification of player info
        ui->nameLineEdit->setText(selectedPlayer->getName());
        ui->factionComboBox->setCurrentIndex(selectedPlayer->getFactionId());
        ui->clubComboBox->setCurrentIndex(ui->clubComboBox->findData(selectedPlayer->getClubDbId()));
        ui->squadPtsSpinBox->setValue(selectedPlayer->getSquadronPoints());
        ui->hasByeCheckBox->setChecked(selectedPlayer->getHasBye());
        ui->addPlayerPushButton->setText("  ");
        ui->addPlayerPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_EditPlayer_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_EditPlayer_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_EditPlayer_Pressed.png);}");
    }
}

void playerInputWindow::on_addClubPushButton_clicked()
{
    bool ok;
    QString clubName = QInputDialog::getText(this, tr("Nouveau club"),
                                             tr("Nom du club :"), QLineEdit::Normal,
                                             "", &ok);
    if (ok && !clubName.isEmpty()) {
        QSqlQuery q(QSqlDatabase::database("dbConnection"));
        q.prepare(QLatin1String("INSERT INTO clubs(clubName) VALUES(?)"));
        q.addBindValue(clubName);
        q.exec();
        refreshClubComboBox();
        ui->clubComboBox->setCurrentIndex(ui->clubComboBox->count() - 1);
    }

}
