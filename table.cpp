#include "table.h"
#include "dbmanager.h"
#include <QtGlobal>
#include <QDebug>

table::table(player* p_player0, player* p_player1, int p_tableId)
{
    player0 = p_player0;
    player1 = p_player1;
    tableId = p_tableId;
    player0Score = 0;
    player1Score = 0;
    isTableRoundOver = false;
}

//Getters
int table::getTableId() {
    return tableId;
}

player* table::getPlayer0() {
    return player0;
}

player* table::getPlayer1() {
    return player1;
}

int table::getPlayer0Score() {
    return player0Score;
}

int table::getPlayer1Score() {
    return player1Score;
}

bool table::getIsTableRoundOver() {
    return isTableRoundOver;
}

int table::getTableDbId() {
    return tableDbId;
}

//Setters
void table::setPlayer(player* p_player, int playerIndex) {
    if (playerIndex == 0)
        player0 = p_player;
    else
        player1 = p_player;
}

void table::setPlayerScores(int p_player0Score, int p_player1Score) {
    player0Score = p_player0Score;
    player1Score = p_player1Score;
    isTableRoundOver = true;
}

void table::setTableDbId(int dbId) {
    tableDbId = dbId;
}

//Other methods
void table::calculateTableRoundResult(int player0PointsDestroyed, int player1PointsDestroyed, int drawWinnerIndex) {
    player0Score = player0PointsDestroyed;
    player1Score = player1PointsDestroyed;

    int scoreDiff = qAbs(player0PointsDestroyed - player1PointsDestroyed);
    int winnerMarginOfVictory, loserMarginOfVictory;

    winnerMarginOfVictory = 100 + scoreDiff;
    loserMarginOfVictory = 100 - scoreDiff;

    if (player0PointsDestroyed > player1PointsDestroyed) {
        player0->addScores(1, winnerMarginOfVictory);
        player1->addScores(0, loserMarginOfVictory);
    } else if (player0PointsDestroyed < player1PointsDestroyed) {
        player0->addScores(0, loserMarginOfVictory);
        player1->addScores(1, winnerMarginOfVictory);
    } else {
        if (drawWinnerIndex == 0) {
            player0->addScores(1, 100);
            player1->addScores(0, 100);
        } else if (drawWinnerIndex == 1) {
            player0->addScores(0, 100);
            player1->addScores(1, 100);
        }
    }
    isTableRoundOver = true;

    emit tableResultsUpdated();
}

void table::calculateTableEliminationRoundResult(int player0PointsDestroyed, int player1PointsDestroyed, int eliminationRoundId) {
    player0Score = player0PointsDestroyed;
    player1Score = player1PointsDestroyed;

    if (player0PointsDestroyed > player1PointsDestroyed) {
        player0->setTopCutRank(32 - tableId);
        player1->setTopCutRank(eliminationRoundId + 1);
    } else {
        player0->setTopCutRank(eliminationRoundId + 1);
        player1->setTopCutRank(32 - tableId);
    }
    isTableRoundOver = true;

    qDebug() << "eliminationRoundId : " + QString::number(eliminationRoundId) + " player0 topCutRank : " + QString::number(player0->getTopCutRank()) + " player1 topCutRank : " + QString::number(player1->getTopCutRank());

    emit tableResultsUpdated();
}

void table::updateDb() {
    QSqlQuery q(QSqlDatabase::database("dbConnection"));
    if (!q.prepare("UPDATE tables SET tablePlayer0Score=" + QString::number(player0Score) + ", tablePlayer1Score=" + QString::number(player1Score) + ", tableRoundIsOver=1 WHERE tableId=" + QString::number(tableDbId))) {
        qDebug() << q.lastError();
    }
    q.exec();
}
