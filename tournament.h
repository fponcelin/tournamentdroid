#ifndef TOURNAMENT_H
#define TOURNAMENT_H

#include "player.h"
#include "gameround.h"
#include <QWidget>
#include <QDate>
#include <QVector>

/*!
 * \brief The tournament class
 *
 * This class manages the tournament.
 * It requires a date, a name and a number of Swiss rounds.
 * It contains a vector of players and a vector of gameRounds and has a method to reorder the players in the vector according to their scores.
 */
class tournament
{
public:
    /*!
     * \brief tournament
     *
     * Class contructor
     * The currentRoundId value is set to 0;
     *
     * \param p_name: the tournament's name
     * \param p_date: the tournament's date
     * \param p_roundNb: the total number of Swiss rounds
     */
    tournament(QString p_name, QDate p_date, int p_roundNb, bool p_hasTopCut);

    //Getters
    /*!
     * \brief getPlayersVector
     * \return playersVector
     */
    QVector<player*> getPlayersVector();

    /*!
     * \brief getName
     * \return name
     */
    QString getName();

    /*!
     * \brief getDate
     * \return date
     */
    QDate getDate();

    /*!
     * \brief getRoundNb
     * \return roundNb
     */
    int getRoundNb();

    /*!
     * \brief getRoundsVector
     * \return roundsVector
     */
    QVector<gameRound*> getRoundsVector();

    /*!
     * \brief getCurrentRoundId
     * \return currentRoundId
     */
    int getCurrentRoundId();

    /*!
     * \brief getTournamentDbId
     * \return tournamentDbId
     */
    int getTournamentDbId();

    /*!
     * \brief getDisplayedRoundId
     * \return the displayedRoundId value
     */
    int getDisplayedRoundId();

    int getTopCutNb();

    bool getHasTopCut();
    bool getIsTournamentOver();
    bool getIsRestoredFromDb();
    //int getRoundDuration(); ***Unused in version 1***

    //Setters  
    /*!
     * \brief setTournamentDbId
     * \param dbId: a database Primary Key value
     */
    void setTournamentDbId(int dbId);

    /*!
     * \brief setDisplayedRoundId
     *
     * Sets the value of displayedRoundId.
     *
     * \param roundId: an integer
     */
    void setDisplayedRoundId(int roundId);

    void setTopCutNb(int p_topCutNb);
    void setCurrentRoundId(int roundId);
    //void setRoundDuration(int p_roundDuration); ***Unused in version 1***

    //Other methods
    /*!
     * \brief addRound
     *
     * This method sets the currentRoundId using the current length of the roundsVector, then creates a gameRound instance and adds it the roundsVector.
     * It then saves the gameRound to the database.
     */
    void addRound();

    /*!
     * \brief addPlayer
     *
     * This method adds a player intance to the playersVector
     *
     * \param newPlayer: a player
     */
    void addPlayer(player * newPlayer);

    /*!
     * \brief deletePlayer
     *
     * This method deletes a player intance from memory and deletes the corresponding reference in the playersVector using playerIndex to identify the player in the vector.
     *
     * \param playerIndex: an integer
     */
    void deletePlayer(int playerIndex);

    /*!
     * \brief sortPlayersVectorDescending
     *
     * This method sorts the players in the playersVector according to their scores (tournament points then margin of victory), using a custom compare method.
     */
    void sortPlayersVectorDescending();

    void addEliminationRound();
    void addDbRestoredRound(gameRound *restoredRound);
    void endTournament(bool isRestoredFromDb = false);
    void restoringFromDb();

    //void startTimer(); ***Unused in version 1***
    //void resetTimer(); ***Unused in version 1***

private:
    QVector<player*> playersVector; /*!< A vector of player object pointers*/
    QString name; /*!< Tournament name*/
    QDate date; /*!< Tournament date*/
    int roundNb; /*!< Tournament total Swiss rounds number*/
    QVector<gameRound*> roundsVector; /*!< A vector of gameRound object pointers*/
    int currentRoundId; /*!< The current round number*/
    int displayedRoundId; /*!< The currently displayed round number*/
    int tournamentDbId; /*!< The tournament's database record Primary Key*/
    bool hasTopCut;
    int topCutNb;
    bool isTournamentOver;
    bool isRestoredFromDb;
    //int roundDuration; ***Unused in version 1***

    void saveRoundToDb(int currentRoundId);
};

#endif // TOURNAMENT_H
