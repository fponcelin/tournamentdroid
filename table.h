#ifndef TABLE_H
#define TABLE_H

#include <QVector>
#include "player.h"

/*!
 * \brief The table class
 *
 * This class manages the tournament gaming tables.
 * It requires two player instances and does the calculation of their tournament points and margin of victory using their scores
 */
class table : public QObject
{
    Q_OBJECT

public:
    /*!
     * \brief table
     *
     * Class contructor
     * The scores are set to 0 and isTableRoundOver set to false.
     *
     * \param p_player0: a player object
     * \param p_player1: a player object
     * \param p_tableId: a table number
     */
    table(player* p_player0, player* p_player1, int p_tableId);

    //Getters
    /*!
     * \brief getTableId
     * \return the table number
     */
    int getTableId();

    /*!
     * \brief getPlayer0
     * \return the first player
     */
    player* getPlayer0();

    /*!
     * \brief getPlayer1
     * \return the second player
     */
    player* getPlayer1();

    /*!
     * \brief getPlayer0Score
     * \return the player0Score value
     */
    int getPlayer0Score();

    /*!
     * \brief getPlayer1Score
     * \return the player1Score value
     */
    int getPlayer1Score();

    /*!
     * \brief getIsTableRoundOver
     * \return the isTableRoundOver boolean value
     */
    bool getIsTableRoundOver();

    int getTableDbId();

    //Setters
    /*!
     * \brief setPlayer1
     *
     * This method sets player1 with a player object pointer passed as parameter.
     *
     * \param p_player1: a player object pointer
     */
    void setPlayer(player* p_player, int playerIndex);

    void setPlayerScores(int p_player0Score, int p_player1Score);
    void setTableDbId(int dbId);

    //Other methods
    /*!
     * \brief calculateTableRoundResult
     *
     * This method calculates the players' score according to FFG X-Wing tournament regulations (dated 15 March 2016).
     * It updates each player's score by calling the addScore method, then sets isTableRoundOver to true.
     * It then emits the tableResultsUpdated signal.
     *
     * \param player0PointsDestroyed: the first player's score (enemy points destroyed)
     * \param player1PointsDestroyed: the second player's score
     */
    void calculateTableRoundResult(int player0PointsDestroyed, int player1PointsDestroyed, int drawWinnerIndex = -1);

    void calculateTableEliminationRoundResult(int player0PointsDestroyed, int player1PointsDestroyed, int eliminationRoundId);

    /*!
     * \brief updateDb
     *
     * This method updates the score values of the table record in the database
     */
    void updateDb();

signals:
    /*!
     * \brief tableResultsUpdated
     *
     * Signal emitted by the calculateTableRoundResult method.
     */
    void tableResultsUpdated();

private:
    int tableId; /*!< The table number*/
    player *player0; /*!< The table's first player*/
    player *player1; /*!< The table's second player*/
    int player0Score; /*!< The first player's score*/
    int player1Score; /*!< The second player's score*/
    bool isTableRoundOver; /*!< *Boolean to indicate if the table's scores have been entered*/
    int tableDbId; /*!< The table database Primary key*/
};

#endif // TABLE_H
