#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QSqlDatabase>
#include <QtSql>
#include "tournament.h"
#include "gameround.h"
#include "table.h"
#include "player.h"

/*!
 * \brief The dbManager class
 *
 * This class manages connection to the database file.
 */
class dbManager
{
public:
    /*!
     * \brief dbManager
     *
     * Constructor
     *
     * \param path: QString of the database file path
     */
    dbManager(const QString &path);

    /*!
     * \brief saveTournamentToDb
     *
     * Method to save a tournament instance including players to the database.
     * The method returns the Primary Key of the database record inserted.
     *
     * \param tournamentToSave: tournament instance
     * \return Primary Key value of the database record inserted
     */
    int saveTournamentToDb(tournament* tournamentToSave);

    /*!
     * \brief addGameRound
     *
     * Method to save a gameRound to the database.
     * The method returns the Primary Key of the database record inserted.
     *
     * \param fk_tournamentId: related tournament Primary Key value
     * \param gameRoundNb: tournament round number
     * \return Primary Key value of the database record inserted
     */
    int addGameRound(int fk_tournamentId, int gameRoundNb);

    /*!
     * \brief addTable
     *
     * Method to save a table with its scores to the database.
     *
     * \param tableToSave: table instance
     * \param fk_gameRoundId: related gameRound Primary Key value
     */
    int addTable(table * tableToSave, int fk_gameRoundId);

private:
    QSqlDatabase db; /*!< Database object to create / connect to */

    /*!
     * \brief addTournament
     *
     * Method that prepares and executes a SQL Insert query for a tournament record.
     *
     * \param q: QSqlQuery instance
     * \param tournamentName: name of the tournament
     * \param tournamentDate: date of the tournament
     * \param tournamentTotalRoundNb: total number of rounds
     * \return Primary Key value of the database tournament record inserted if successful, false otherwise
     */
    QVariant addTournament(QSqlQuery &q, const QString &tournamentName, const QDate &tournamentDate, int tournamentTotalRoundNb, int tournamentHasTopCut, int tournamentTopCutNb);

    /*!
     * \brief addPlayer
     *
     * Method that prepares and executes a SQLite Insert query for a player record
     *
     * \param q: QSqlQuery instance
     * \param fk_tournamentId: Primary Key value of the related tournament
     * \param playerName: name of the player
     * \param playerFactionId: player's faction number
     * \param playerSquadronPoints: player's squadron points value
     * \param playerTournamentPoints: player's tournament points
     * \param playerMarginOfVictory: players's margin of victory
     * \return Primary Key value of the database player record inserted
     */
    int addPlayer(QSqlQuery &q, const QVariant &fk_tournamentId, const QString &playerName, int playerFactionId, int playerSquadronPoints, int playerTournamentPoints, int playerMarginOfVictory, int playerClubDbId);

};

#endif // DBMANAGER_H
