#include "tournament.h"
#include "dbmanager.h"
#include <QDebug>
#include <QtMath>

tournament::tournament(QString p_name, QDate p_date, int p_roundNb, bool p_hasTopCut)
{
    name = p_name;
    date = p_date;
    roundNb = p_roundNb;
    currentRoundId = 0;
    displayedRoundId = 0;
    hasTopCut = p_hasTopCut;
    topCutNb = 0;
    isTournamentOver = false;
    isRestoredFromDb = false;
}

//Getters
QVector<player*> tournament::getPlayersVector() {
    return playersVector;
}

QString tournament::getName() {
    return name;
}

QDate tournament::getDate() {
    return date;
}

int tournament::getRoundNb() {
    return roundNb;
}

QVector<gameRound*> tournament::getRoundsVector() {
    return roundsVector;
}

int tournament::getCurrentRoundId() {
    return currentRoundId;
}

int tournament::getTournamentDbId() {
    return tournamentDbId;
}

int tournament::getDisplayedRoundId() {
    return displayedRoundId;
}

int tournament::getTopCutNb() {
    return topCutNb;
}

bool tournament::getHasTopCut() {
    return hasTopCut;
}

bool tournament::getIsTournamentOver() {
    return isTournamentOver;
}

bool tournament::getIsRestoredFromDb() {
    return isRestoredFromDb;
}

/*
int tournament::getRoundDuration() {
    return roundDuration
}*/

//Setters
void tournament::setTournamentDbId(int dbId) {
    tournamentDbId = dbId;
}

void tournament::setDisplayedRoundId(int roundId) {
    displayedRoundId = roundId;
}

void tournament::setTopCutNb(int p_topCutNb) {
    topCutNb = p_topCutNb;
}

void tournament::setCurrentRoundId(int roundId) {
    currentRoundId = roundId;
}

/*Unused in version 1
void tournament::setRoundDuration(int p_roundDuration) {
    roundDuration = p_roundDuration;
}*/

//Other methods
void tournament::addPlayer(player * newPlayer) {
    playersVector.push_back(newPlayer);
}

void tournament::deletePlayer(int playerIndex) {
    delete playersVector.at(playerIndex);
    playersVector.removeAt(playerIndex);
}

bool comparePlayersScores(player* a, player* b) {
    if (a->getTopCutRank() > b->getTopCutRank()) {
        return true;
    } else {
        if (a->getTournamentPoints() > b->getTournamentPoints() && a->getTopCutRank() == b->getTopCutRank()) {
            return true;
        } else {
            if (a->getMarginOfVictory() > b->getMarginOfVictory() && a->getTournamentPoints() == b->getTournamentPoints() && a->getTopCutRank() == b->getTopCutRank()) {
                return true;
            } else {
                return false;
            }
        }
    }
}

void tournament::sortPlayersVectorDescending() {
    std::sort(playersVector.begin(), playersVector.end(), comparePlayersScores);
}

void tournament::addRound() {
    currentRoundId = roundsVector.length();
    displayedRoundId = currentRoundId;
    roundsVector.push_back(new gameRound(playersVector, currentRoundId, false));

    //Save gameRound to database
    saveRoundToDb(currentRoundId);
}

void tournament::addEliminationRound() {
    currentRoundId = roundsVector.length();
    displayedRoundId = currentRoundId;

    int playersTopCutNb = qPow(2, topCutNb - (currentRoundId - roundNb));
    QVector<player*> topCutPlayersVector;

    for (int i = 0; i < playersTopCutNb; i++) {
        topCutPlayersVector.append(playersVector[i]);
    }
    roundsVector.push_back(new gameRound(topCutPlayersVector, currentRoundId, true));

    //Save gameRound to database
    saveRoundToDb(currentRoundId);
}

void tournament::addDbRestoredRound(gameRound *restoredRound) {
    roundsVector.append(restoredRound);
}

void tournament::endTournament(bool isRestoredFromDb) {
    isTournamentOver = true;
    if (!isRestoredFromDb) {
        QSqlQuery q(QSqlDatabase::database("dbConnection"));
        if (!q.prepare("UPDATE tournaments SET tournamentIsOver=1 WHERE tournamentId=" + QString::number(tournamentDbId))) {
            qDebug() << q.lastError();
        }
        q.exec();
    }
}

void tournament::restoringFromDb() {
    isRestoredFromDb = true;
}

/*Unused in version 1
void tournament::startTimer() {
    //This asynchronous method will start a time countdown starting from the value set in roundDuration
}

void tournament::resetTimer() {
    //This method will reset the timer at the value set in roundDuration
}*/

void tournament::saveRoundToDb(int currentRoundId) {
    QSettings settings("Florian Poncelin", "TournamentDroid");
    dbManager db(settings.value("databasePath").toString());
    qDebug() << tournamentDbId;
    roundsVector[currentRoundId]->setRoundDbId(db.addGameRound(tournamentDbId, currentRoundId));

    //Save gameRound tables to database
    for (table *t : roundsVector[currentRoundId]->getTablesVector()) {
        qDebug() << "About to add table to DB";
        qDebug() << "Player0Name : " + t->getPlayer0()->getName();
        int tableDbId = db.addTable(t, roundsVector[currentRoundId]->getRoundDbId());
        t->setTableDbId(tableDbId);
    }
}
