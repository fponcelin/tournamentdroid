#ifndef TOURNAMENTWINDOW_H
#define TOURNAMENTWINDOW_H

#include <QWidget>
#include <QWidget>
#include <QStringList>
#include <QStringListModel>
#include <QAbstractItemView>
#include <QStandardItemModel>
#include "tournament.h"
#include "playersrankingslistviewdelegate.h"
#include "tableslistviewdelegate.h"
#include "tableresultsinputdialog.h"
#include "dbmanager.h"

namespace Ui {
class tournamentWindow;
}

/*!
 * \brief The tournamentWindow class
 *
 * This class manages the user interface for the tournament running.
 * It requires a tournament instance and displays all related information to the user.
 */
class tournamentWindow : public QWidget
{
    Q_OBJECT

public:
    /*!
     * \brief tournamentWindow
     *
     * Class constructor
     *
     * \param parent
     */
    explicit tournamentWindow(QWidget *parent = 0);
    ~tournamentWindow();

public slots:
    /*!
     * \brief setCurrentTournament
     *
     * Slot receiving a tournament instance.
     *
     * This method assigns the received tournament pointer to the currentTournament member then updates all UI elements using the tournament information.
     * It then emits the receivedTournament signal.
     *
     * \param p_currentTournament
     */
    void setCurrentTournament(tournament *p_currentTournament, bool restoredFromDb = false);

    /*!
     * \brief on_finishTournamentPushButton_clicked
     *
     * Slot receiving the finishTournamentPushButton click signal.
     *
     * This method creates a QMessageBox to ask the user if he wants to save his tournament or not to the database and return to the welcomeWindow or cancel and return to the tournamentWindow.
     * If the user didn't cancel it emits the tournamentEnded signal.
     */
    void on_finishTournamentPushButton_clicked();

signals:
    /*!
     * \brief receivedTournament
     *
     * Signal emitted by the setCurrentTournament method.
     */
    void receivedTournament();

    /*!
     * \brief tournamentEnded
     *
     * Signal emitted by the on_finishTournamentPushButton_clicked if not canceled.
     */
    void tournamentEnded();

private slots:
    /*!
     * \brief on_newGameRoundPushButton_clicked
     *
     * Slot receiving the newGameRoundPushButton click signal.
     *
     * This method checks if all the gameRound tables have received their scoring.
     * If not it will call the warnUserTableRoundIsNotOver method.
     * If yes it will check if the tournament hasn't reached the last Swiss round.
     * If the end is reached it will call the warnUserTournamentIsOver method.
     * Otherwise it will create a new gameRound by calling the tournament addRound method.
     */
    void on_newGameRoundPushButton_clicked();

    /*!
     * \brief saveTableToDb
     *
     * Slot receiving the signal from the tableResultsInputDialog when a table's scores have been entered with the corresponding table pointer passed on.
     * This method saves the received table information to the database.
     *
     * \param updatedTable
     */
    void saveTableToDb(table *updatedTable);

private:
    Ui::tournamentWindow *ui; /*!< User interface object*/
    tournament *currentTournament; /*!< Tournament object*/
    QStandardItemModel *playersRankingsListViewModel; /*!< List model to display the players rankings data*/
    QStandardItemModel *tablesListViewModel; /*!< List model to display the tournament tables data*/
    playersRankingsListViewDelegate *currentPlayersListRankingsViewDelegate; /*!< Delegate managing the currentPlayersListView line formatting*/
    tablesListViewDelegate *currentTablesListViewDelegate; /*!< Delegate managing the tablesListView line formatting*/
    tableResultsInputDialog *resultsInputDialog; /*!< Widget to input the results of a table*/

private slots:
    /*!
     * \brief refreshPlayersRankings
     *
     * This method calls the tournament sortPlayersVectorDescending method, then clears the playersRankingsListView and fills it with the updated information.
     */
    void refreshPlayersRankings();

    /*!
     * \brief refreshTablesListView
     *
     * This method clears the tablesListView and fills it with updated information.
     */
    void refreshTablesListView();

    /*!
     * \brief warnUserTableRoundIsNotOver
     *
     * This method creates a QMessageBox to inform the user that some tables have not received their scores.
     */
    void warnUserTableRoundIsNotOver();

    /*!
     * \brief warnUserTournamentIsOver
     *
     * This method creates a QMessageBox to inform the user the tournament has reached its last Swiss round and can't create a new gameRound.
     */
    void warnUserTournamentIsOver();

    /*!
     * \brief on_finishTablePushButton_clicked
     *
     * Slot receiving the finishTablePushButton click signal.
     *
     * This method creates a new tableResultsInputDialog, passing on a table using the selected tablesListView row to retrieve the table in the gameRound's tablesVector.
     * It then disables the finishTablePushButton to prevent clicking unless a row is selected.
     *
     * If the current tournament's displayedRoundId value differs from the currentRoundId value, it increments the displayedRoundId value instead, then calls the refreshTablesListView method.
     */
    void on_finishTablePushButton_clicked();

    /*!
     * \brief on_tablesListView_clicked
     *
     * Slot receiving the tablesListView click signal when a row is selected.
     *
     * This method enables the finishTablePushButton to allow the input of table results for the selected row.
     *
     * \param index
     */
    void on_tablesListView_clicked(const QModelIndex &index);

    /*!
     * \brief on_previousRoundPushButton_clicked
     *
     * Slot receiving the previousRoundPushButton click signal.
     * This method decrements the current tournament's displayedRoundId value, calls the refreshTablesListView method and changes the finishTablePushButton text.
     */
    void on_previousRoundPushButton_clicked();

    void quitTournament();
    void openTableResultsInputDialog();
    void on_tablesListView_doubleClicked(const QModelIndex &index);
};

#endif // TOURNAMENTWINDOW_H
