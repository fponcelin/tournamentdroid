#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStackedWidget>
#include "welcomewindow.h"
#include "tournamentwindow.h"
#include "manageclubswindow.h"

namespace Ui {
class MainWindow;
}

/*!
 * \brief The MainWindow class
 *
 * Class managing the main window, in which the two main QWidgets appear.
 * The widgets are contained in a QStackedWidget to switch between them easily.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     * \brief MainWindow
     *
     * Class constructor
     *
     * \param parent
     */
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void closeEvent(QCloseEvent *event);

public slots:
    /*!
     * \brief tournamentStarts
     *
     * Slot receiving a signal when the tournament starts after all players have been added.
     * This method sets the index of the QStackedWidget to 1 to display the tournamentWindow widget.
     */
    void tournamentStarts();

    /*!
     * \brief tournamentEnds
     *
     * Slot receiving a signal when the tournament has ended.
     * This method sets the index of the QStackedWidget to 0 to display the welcomeWindow widget.
     */
    void tournamentEnds();

private:
    Ui::MainWindow *ui; /*!< User interface object*/
    welcomeWindow *welcomew; /*!< Widget of the welcome screen*/
    tournamentWindow *tournamentw; /*!< Widget of the tournament management screen*/
    manageClubsWindow *manageClubsw; /*!< Widget of the clubs management window*/
    QStackedWidget *mainWidget; /*!< Stacked widget containing the two widgets above*/

private slots:
    /*!
     * \brief openHelpFile
     *
     * Slot receiving the actionUserGuide menu item triggered signal.
     * This method opens a pdf file of the user guide.
     */
    void openHelpFile();

    /*!
     * \brief quitApp
     *
     * Slot receiving the actionQuit menu item triggered signal.
     * This method will call the on_finishTournamentPushButton_clicked method from tournamentWindow to ask the user if he wants to save his tournament.
     * Unless the user choses to cancel, the app will return to the welcomeWindow and quit immediately after.
     */
    void quitApp();

    void openManageClubs();
    void openAbout();
    void drawBackgroundImg();
    void resizeEvent(QResizeEvent *evt);
};

#endif // MAINWINDOW_H
