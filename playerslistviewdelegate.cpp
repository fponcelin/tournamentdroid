#include "playerslistviewdelegate.h"
#include <QDebug>

QSize playersListViewDelegate::iconSize = QSize(30, 30);
int playersListViewDelegate::padding = 5;

playersListViewDelegate::playersListViewDelegate()
{

}

QSize playersListViewDelegate::sizeHint(const QStyleOptionViewItem &  option ,
                                        const QModelIndex & index) const
{
    if(!index.isValid())
        return QSize();

    QString headerText = index.data(PlayerName).toString();
    QString subheaderText = index.data(PlayerSquadPts).toString();

    QFont headerFont = QApplication::font();
    headerFont.setBold(true);
    headerFont.setPointSize(16);
    QFont subheaderFont = QApplication::font();
    QFontMetrics headerFm(headerFont);
    QFontMetrics subheaderFm(subheaderFont);

    /* No need for x,y here. we only need to calculate the height given the width.
     * Note that the given height is 0. That is because boundingRect() will return
     * the suitable height if the given geometry does not fit. And this is exactly
     * what we want.
     */
    QRect headerRect = headerFm.boundingRect(0, 0,
                                             option.rect.width() - iconSize.width(), 0,
                                             Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                             headerText);
    QRect subheaderRect = subheaderFm.boundingRect(0, 0,
                                                   option.rect.width() - iconSize.width(), 0,
                                                   Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                                   subheaderText);

    QSize size(option.rect.width(), headerRect.height() + subheaderRect.height() +  2*padding);

    /* Keep the minimum height needed in mind. */
    if(size.height()<iconSize.height())
        size.setHeight(iconSize.height());

    return size;
}

void playersListViewDelegate::paint(QPainter *painter,
                                    const QStyleOptionViewItem &option,
                                    const QModelIndex &index) const
{
    if(!index.isValid())
        return;

    painter->save();

    if (option.state & QStyle::State_Selected)
        painter->fillRect(option.rect, option.palette.highlight());

    QString headerText = index.data(PlayerName).toString();
    QString subheaderText = index.data(PlayerSquadPts).toString() + " points";
    QString clubText = " - " + index.data(PlayerClub).toString();
    QString byeText = index.data(PlayerBye).toString();

    QFont headerFont = QApplication::font();
    headerFont.setBold(true);
    headerFont.setPointSize(16);
    QFont subheaderFont = QApplication::font();
    QFont clubFont = QApplication::font();
    QFont byeFont = QApplication::font();
    byeFont.setBold(true);
    QFontMetrics headerFm(headerFont);
    QFontMetrics subheaderFm(subheaderFont);
    QFontMetrics clubFm(clubFont);
    QFontMetrics byeFm(byeFont);

    /*
     * The x,y coords are not (0,0) but values given by 'option'. So, calculate the
     * rects again given the x,y,w.
     * Note that the given height is 0. That is because boundingRect() will return
     * the suitable height if the given geometry does not fit. And this is exactly
     * what we want.
     */

    QRect headerRect =
            headerFm.boundingRect(option.rect.left() + iconSize.width() + padding * 2, option.rect.top() + padding,
                                  option.rect.width() - iconSize.width(), 0,
                                  Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                  headerText);
    QRect subheaderRect =
            subheaderFm.boundingRect(headerRect.left(), headerRect.bottom(),
                                     option.rect.width() - iconSize.width(), 0,
                                     Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                     subheaderText);

    QRect clubRect =
            clubFm.boundingRect(subheaderRect.right(), subheaderRect.top(),
                                option.rect.width() - iconSize.width(), 0,
                                Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                clubText);

    QRect byeRect =
            byeFm.boundingRect(clubRect.right(), subheaderRect.top(),
                               option.rect.width() - iconSize.width(), 0,
                               Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                               byeText);

    QImage icon = QImage(":/img/" + index.data(PlayerFaction).toString() + ".png");
    QRect imageRect = QRect(option.rect.left() + padding, option.rect.top() + option.rect.height()/2 - 15, icon.width(), iconSize.height());
    painter->drawImage(imageRect, icon);

    painter->setPen(Qt::black);

    painter->setFont(headerFont);
    painter->drawText(headerRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, headerText);

    painter->setFont(subheaderFont);
    painter->drawText(subheaderRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, subheaderText);

    painter->setFont(clubFont);
    painter->drawText(clubRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, clubText);

    painter->setFont(byeFont);
    painter->drawText(byeRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, byeText);

    painter->restore();
}
