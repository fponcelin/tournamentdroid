#ifndef PLAYERSRANKINGSLISTVIEWDELEGATE_H
#define PLAYERSRANKINGSLISTVIEWDELEGATE_H

#include <QStyledItemDelegate>
#include <QLabel>
#include <QPainter>
#include <QApplication>
#include <QImage>

/*!
 * \brief The playersRankingsListViewDelegate class
 *
 * This class manages the formatting of each line in the playersRankingsListView
 */
class playersRankingsListViewDelegate : public QStyledItemDelegate
{
public:
    enum datarole { PlayerName = Qt::UserRole + 1, PlayerFaction, PlayerTournamentPts, PlayerMoV, PlayerIndex, TopCutBracket };

    playersRankingsListViewDelegate();

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index ) const;

    static QSize iconSize;
    static int padding;
};

#endif // PLAYERSRANKINGSLISTVIEWDELEGATE_H
