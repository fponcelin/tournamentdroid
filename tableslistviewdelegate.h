#ifndef TABLESLISTVIEWDELEGATE_H
#define TABLESLISTVIEWDELEGATE_H

#include <QStyledItemDelegate>
#include <QLabel>
#include <QPainter>
#include <QApplication>

/*!
 * \brief The tablesListViewDelegate class
 *
 * This class manages the formatting of each line in the tablesListView
 */
class tablesListViewDelegate : public QStyledItemDelegate
{
public:
    enum datarole { TableId = Qt::UserRole + 1, Player0Name, Player0PtsDestroyed, Player1Name, Player1PtsDestroyed };

    tablesListViewDelegate();

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index ) const;

    static int padding;
};

#endif // TABLESLISTVIEWDELEGATE_H
