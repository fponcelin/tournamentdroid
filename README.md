# Project name #

TournamentDroid is an open source Windows desktop application aimed at X-Wing Miniatures tournament organisers.

## Description ##

Its purpose is to provide a user-friendly tool to manage the running of a tournament, pair players together and calculate their scores. It follows the rules of X-Wing Miniatures tournaments as provided by Fantasy Flight Games.

This program has been written in C++ with Qt Open Source.

## Contributing ##

If you wish to improve on my code or have fun with it, you'll need a copy of the Qt IDE. You can download a copy at https://www.qt.io/download/

## License ##

Copyright 2016 Florian Poncelin.

Licensed under the GNU Lesser Public License.

TournamentDroid is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

TournamentDroid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with TournamentDroid. If not, see <http://www.gnu.org/licenses/>.