#ifndef PLAYERSLISTVIEWDELEGATE_H
#define PLAYERSLISTVIEWDELEGATE_H

#include <QStyledItemDelegate>
#include <QLabel>
#include <QPainter>
#include <QApplication>
#include <QImage>

/*!
 * \brief The playersListViewDelegate class
 *
 * This class manages the formatting of each line in the playersListView
 */
class playersListViewDelegate : public QStyledItemDelegate
{
public:
    enum datarole { PlayerName = Qt::UserRole + 1, PlayerFaction, PlayerSquadPts, PlayerClub, PlayerBye };

    playersListViewDelegate();

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index ) const;

    static QSize iconSize;
    static int padding;

private:

};

#endif // PLAYERSLISTVIEWDELEGATE_H
