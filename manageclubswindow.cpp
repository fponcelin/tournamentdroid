#include "manageclubswindow.h"
#include "ui_manageclubswindow.h"
#include <QDebug>
#include <QPalette>
#include <QPainter>

manageClubsWindow::manageClubsWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::manageClubsWindow)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    drawBackgroundImg();
    ui->addClubPushButton->setText("");
    ui->addClubPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_AddClub_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_AddClub_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_AddClub_Pressed.png);}");
    ui->modifyClubPushButton->setText("");
    ui->modifyClubPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_EditClub_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_EditClub_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_EditClub_Pressed.png);} QPushButton::disabled {image: url(:/img/Btn_EditClub_Disabled.png);}");
    ui->deleteClubPushButton->setText("");
    ui->deleteClubPushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_DeleteSelectedClub_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_DeleteSelectedClub_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_DeleteSelectedClub_Pressed.png);} QPushButton::disabled {image: url(:/img/Btn_DeleteSelectedClub_Disabled.png);}");
    ui->closePushButton->setText("");
    ui->closePushButton->setStyleSheet("QPushButton {image: url(:/img/Btn_CloseClub_Normal.png);background-color: rgba(0,0,0,0);border:none;} QPushButton::hover {image: url(:/img/Btn_CloseClub_Hover.png);} QPushButton::pressed {image: url(:/img/Btn_CloseClub_Pressed.png);}");
    refreshClubsList();
}

manageClubsWindow::~manageClubsWindow()
{
    delete ui;
}

void manageClubsWindow::refreshClubsList() {
    ui->clubsListWidget->clear();
    QSqlQuery q(QLatin1String("SELECT * FROM clubs WHERE clubId > 1"), QSqlDatabase::database("dbConnection"));
    while (q.next()) {
        QListWidgetItem *item = new QListWidgetItem(q.value("clubName").toString());
        item->setData(Qt::UserRole, q.value("clubId").toInt());
        ui->clubsListWidget->addItem(item);
    }
}

void manageClubsWindow::resetButtons() {
    ui->clubNameLineEdit->setText("");
    ui->modifyClubPushButton->setEnabled(false);
    ui->deleteClubPushButton->setEnabled(false);
}

void manageClubsWindow::on_closePushButton_clicked()
{
    this->close();
}

void manageClubsWindow::on_clubsListWidget_itemClicked(QListWidgetItem *item)
{
    ui->clubNameLineEdit->setText(item->text());
    selectedClubDbId = item->data(Qt::UserRole).toInt();
    ui->modifyClubPushButton->setEnabled(true);
    ui->deleteClubPushButton->setEnabled(true);
}

void manageClubsWindow::on_modifyClubPushButton_clicked()
{
    if (!ui->clubNameLineEdit->text().isEmpty()) {
        QSqlQuery q(QSqlDatabase::database("dbConnection"));
        q.prepare("UPDATE clubs SET clubName=:name WHERE clubId=:id");
        q.bindValue(":name", ui->clubNameLineEdit->text());
        q.bindValue(":id", selectedClubDbId);
        q.exec();
        resetButtons();
        refreshClubsList();
    }
}

void manageClubsWindow::on_deleteClubPushButton_clicked()
{
    QSqlQuery q(QSqlDatabase::database("dbConnection"));
    q.prepare("DELETE FROM clubs WHERE clubId=:id");
    q.bindValue(":id", selectedClubDbId);
    q.exec();
    resetButtons();
    refreshClubsList();
}

void manageClubsWindow::on_addClubPushButton_clicked()
{
    if (!ui->newClubLineEdit->text().isEmpty()) {
        QSqlQuery q(QSqlDatabase::database("dbConnection"));
        q.prepare(QLatin1String("INSERT INTO clubs(clubName) VALUES(?)"));
        q.addBindValue(ui->newClubLineEdit->text());
        q.exec();
        ui->newClubLineEdit->setText("");
        refreshClubsList();
    }
}

void manageClubsWindow::drawBackgroundImg() {
    QImage BgTile(":/img/Bg_PlayerInput_Fill.png");
    QImage BgTopImg(":/img/Bg_PlayerInput_Top.png");
    QImage BgBottomImg(":/img/Bg_PlayerInput_Bottom.png");

    QPixmap bgImg(this->size());
    QPainter paint(&bgImg);
    for (int i=0; i<this->height(); i+=50) {
        paint.drawImage(this->rect().left(),i,BgTile);
    }
    paint.drawImage(this->rect().left(),this->rect().top(),BgTopImg);
    paint.drawImage(this->rect().left(),this->rect().bottom() - 49,BgBottomImg);

    QPalette palette = this->palette();
    palette.setBrush(QPalette::Background, bgImg);//set the pic to the background
    this->setPalette(palette); //show the background pic
}
