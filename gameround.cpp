#include "gameround.h"
#include <QDebug>
#include <QtGlobal>
#include <QMessageBox>

gameRound::gameRound(QVector<player*> tournamentPlayersVector, int p_roundId, bool isEliminationRound, bool isRestoredFromDb)
{
    isRoundOver = false;
    roundId = p_roundId;
    playersVector = tournamentPlayersVector;

    //Update the previous score values of all players to allow score changing after input
    for (player* p : playersVector) {
        p->updatePreviousScores();
    }

    if (!isRestoredFromDb) {
        if (!isEliminationRound) {
            generateTablesVector();
        } else {
            generateTopCutPairing();
        }
    }
}

//Getters
QVector<table*> gameRound::getTablesVector() {
    return tablesVector;
}

int gameRound::getRoundId() {
    return roundId;
}

bool gameRound::getIsRoundOver() {
    return isRoundOver;
}

int gameRound::getRoundDbId() {
    return roundDbId;
}


//Setters
void gameRound::setRoundDbId(int dbId) {
    roundDbId = dbId;
}


//Other methods
void gameRound::roundIsOver() {
    isRoundOver = true;
}

void gameRound::badFeeling() {
    QMessageBox msgBox;
    msgBox.setText("I have a bad feeling about this...\n\nThis app can't handle a crash of that magnitude!\nWe're doomed!!!");
    msgBox.exec();
}

void gameRound::warnClubsIgnored() {
    QMessageBox msgBox;
    msgBox.setText(QObject::tr("Des joueurs du même club ont été placés ensemble.\nJe suis désolé mais je ne pouvais pas faire autrement !"));
    msgBox.exec();
}

void gameRound::addDbRestoredTable(table * restoredTable) {
    tablesVector.append(restoredTable);
}

void gameRound::pairPlayers(int player0Index, int player1Index, int tableId) {
    tablesVector.append(new table(tempPlayers[player0Index], tempPlayers[player1Index], tableId));
    tempPlayers[player0Index]->addPreviousOpponentId(tempPlayers[player1Index]->getPlayerDbId());
    tempPlayers[player1Index]->addPreviousOpponentId(tempPlayers[player0Index]->getPlayerDbId());

    if (player1Index > player0Index) {
        tempPlayers.removeAt(player1Index);
        tempPlayers.removeAt(player0Index);
    } else {
        tempPlayers.removeAt(player0Index);
        tempPlayers.removeAt(player1Index);
    }
}

bool gameRound::generateFirstRoundPairings(bool disregardClubs) {
    bool isSuccessful = true;
    int tableId = 0;
    int player0Index;
    int player1Index;
    int loopCount = 0; //Loop counter with break to exit after 100 loops (safety measure to avoid infinite looping)

    if (tablesVector.size() > 0) {
        for (int i = 0; i < tablesVector.size(); i++) {
            delete tablesVector[i];
        }
    }

    tablesVector.clear();
    tempPlayers.clear();
    tempPlayers = playersVector;

    //Loop to assign players to tables randomly for round 0
    while (tempPlayers.size() > 0) {
        player0Index = 0;
        player1Index = (qrand() % (tempPlayers.size() - 1)) + 1;

        if (tempPlayers[player0Index]->getClubDbId() != tempPlayers[player1Index]->getClubDbId() || tempPlayers[player0Index]->getClubDbId() == 1) {
                pairPlayers(player0Index, player1Index, tableId);
                tableId++;
        } else {
            loopCount++;
            if (loopCount >= 100) {
                if (disregardClubs && isSuccessful) {
                    pairPlayers(player0Index, player1Index, tableId);
                    tableId++;
                } else {
                    isSuccessful = false;
                    break;
                }
            }
        }
    }
    return isSuccessful;
}

bool gameRound::generateSubsequentRoundsPairing() {
    bool isSuccessful = true;
    int tableId = 0;
    int player0Index;
    int player1Index;
    int loopCount = 0; //Loop counter with break to exit after 100 loops (safety measure to avoid infinite looping)

    //Loop to assign players to tables according to their scores
    while (playersVector.size() > 0 || tempPlayers.size() > 0) {
        int topScore = playersVector.first()->getTournamentPoints();
        int playerCounter = 0;
        bool playersAreAssigned = false;

        //Loop to fill tempPlayers with players from remaining top scoring bracket
        qDebug() << "About to loop to fill tempPlayers";
        for (player* p : playersVector) {
            if (p->getTournamentPoints() == topScore) {
                qDebug() << "Adding " + p->getName() + " to tempPlayers";
                tempPlayers.append(p);
                playerCounter++;
            }
        }

        //Loop to remove the previously selected players from playersVector
        qDebug() << "About to remove players from playersVector";
        for (int i = 0; i < playerCounter; i++) {
            qDebug() << "Removing " + playersVector.first()->getName() + " from playersVector";
            playersVector.removeFirst();
        }

        //Loop to assign selected players to tables
        qDebug() << "About to enter loop to assign tempPlayers to tables";
        while (tempPlayers.size() > 1) {
            //bool playersHaveFacedPreviously = false;
            player0Index = 0;
            player1Index = (qrand() % (tempPlayers.size() - 1)) + 1;
            qDebug() << "tempPlayers size : " + QString::number(tempPlayers.size());
            qDebug() << "player1Index : " + QString::number(player1Index);

            //Compare previous opponents and prevent illegal pairing
            /*qDebug() << "About to compare previous opponents";
            if (tempPlayers[player0Index]->getPreviousOpponentsIdVector().contains(tempPlayers[player1Index]->getPlayerDbId())) {
                playersHaveFacedPreviously = true;
                qDebug() << "Players have faced previously";
            }

            for (int previousOpponentId : tempPlayers[player0Index]->getPreviousOpponentsIdVector()) {
                qDebug() << "Checking previous opponents for " + tempPlayers[player0Index]->getName();
                qDebug() << "The opponent ID is : " + QString::number(tempPlayers[player1Index]->getPlayerDbId());
                qDebug() << "The current PreviousOpponentIdVector value is : " + QString::number(previousOpponentId);
                if (previousOpponentId == tempPlayers[player1Index]->getPlayerDbId()) {
                    playersHaveFacedPreviously = true;
                    qDebug() << "Players have faced previously";
                }
            }*/

            if (!tempPlayers[player0Index]->getPreviousOpponentsIdVector().contains(tempPlayers[player1Index]->getPlayerDbId())) { //Players have not yet played together, assigning to a table
                qDebug() << "Players have not faced previously, assigning to table";
                pairPlayers(player0Index, player1Index, tableId);
                tableId++;
                playersAreAssigned = true;
                isSuccessful = true;

            } else { //Players have previously faced each other
                qDebug() << "Players have faced each other";
                if (tempPlayers.size() >= 2 && playersVector.size() >= 0) { //Break from loop if only 2 players remaining in tempPlayers to include next scoring bracket
                    loopCount++;
                    if (loopCount < 50) //continue looping to try with different player1Index
                        continue;
                    else {
                        loopCount = 0;
                        if (playersVector.size() > 0)
                            break;
                    }
                }

                if (tempPlayers.size() > 0 && playersVector.size() == 0) { //Remaining players unassigned, loop through previous tables to find unfaced player and swap
                    qDebug() << "Swapping players from previous tables, going to loop";
                    for (int i = tablesVector.size() - 1; i > 0; i--) {
                        if (!tempPlayers.first()->getPreviousOpponentsIdVector().contains(tablesVector[i]->getPlayer0()->getPlayerDbId())) {
                            //Change last previousOpponentsId from table players
                            tablesVector[i]->getPlayer0()->getPreviousOpponentsIdVector().removeLast();
                            tablesVector[i]->getPlayer0()->addPreviousOpponentId(tempPlayers.first()->getPlayerDbId());
                            tablesVector[i]->getPlayer1()->getPreviousOpponentsIdVector().removeLast();

                            //Append tablePlayer1 to tempPlayers and swap with tempPlayers.first
                            tempPlayers.append(tablesVector[i]->getPlayer1());
                            tablesVector[i]->setPlayer(tempPlayers.first(), 1);
                            tempPlayers.removeFirst();
                            break;
                        } else if (!tempPlayers.first()->getPreviousOpponentsIdVector().contains(tablesVector[i]->getPlayer1()->getPlayerDbId())) {
                            //Change last previousOpponentsId from table players
                            tablesVector[i]->getPlayer1()->getPreviousOpponentsIdVector().removeLast();
                            tablesVector[i]->getPlayer1()->addPreviousOpponentId(tempPlayers.first()->getPlayerDbId());
                            tablesVector[i]->getPlayer0()->getPreviousOpponentsIdVector().removeLast();

                            tempPlayers.append(tablesVector[i]->getPlayer0());
                            tablesVector[i]->setPlayer(tempPlayers.first(), 0);
                            tempPlayers.removeFirst();
                            break;
                        }
                    }
                    for (int i = 0; i < tempPlayers.size() - 1; i++) {
                        qDebug() << "Trying to pair player taken from previous table to remaining player";
                        if (!tempPlayers[i]->getPreviousOpponentsIdVector().contains(tempPlayers.last()->getPlayerDbId())) {
                            pairPlayers(tempPlayers.size() - 1, i, tableId);
                            tableId++;
                            playersAreAssigned = true;
                            isSuccessful = true;
                        } else {
                            playersAreAssigned = false;
                        }
                    }

                    /*player *playerToRelocate = tablesVector.last()->getPlayer1();
                    tablesVector.last()->setPlayer1(tempPlayers[player0Index]);
                    tablesVector.append(new table(playerToRelocate, tempPlayers[player1Index], tableId));
                    tempPlayers.removeAt(player1Index);
                    tempPlayers.removeAt(player0Index);
                    playersAreAssigned = true;
                    isSuccessful = true;*/
                } else {
                    playersAreAssigned = false;
                }
            }

            if (!playersAreAssigned) {
                loopCount++;
                if (loopCount == 100) {
                    isSuccessful = false;
                    break;
                }
            }
        }
        if (!isSuccessful) {
            break;
        }
    }
    return isSuccessful;
}


void gameRound::generateTablesVector() {
    if (!roundId) { //Round 0
        //Check for players with a first round bye
        for (int i = playersVector.size() - 1; i >= 0; i--) {
            if (playersVector[i]->getHasBye()) {
                playersVector[i]->addScores(1, 150);
                playersVector.removeAt(i);
            }
        }

        //Check if odd number of players and assign bye to a random one
        if (playersVector.size() % 2 != 0) {
            int randomPlayerIndex = qrand() % playersVector.size();
            playersVector[randomPlayerIndex]->addScores(1, 150);
            playersVector[randomPlayerIndex]->receiveBye();
            playersVector.removeAt(randomPlayerIndex);
        }

        for (int i = 0; i < 3; i++) {
            if (generateFirstRoundPairings(false)) {
                qDebug() << "Players have been paired, all good";
                break;
            } else if (i == 2) {
                if (generateFirstRoundPairings(true)) {
                    qDebug() << "Players from same club have been paired";
                    warnClubsIgnored();
                } else {
                    qDebug() << "WE'RE DOOMED!!!!";
                    badFeeling();
                }
            }
        }
    } else { //Round 1 and beyond
        //Check if odd number of players and assign bye to last one who didn't already received a bye
        if (playersVector.size() % 2 != 0) {
            bool byeGiven = false;
            int lowRankingPlayerIndex = playersVector.size() - 1;
            while (!byeGiven) {
                if (!playersVector[lowRankingPlayerIndex]->getHasBye()) {
                    playersVector[lowRankingPlayerIndex]->addScores(1, 150);
                    playersVector[lowRankingPlayerIndex]->receiveBye();
                    playersVector.removeAt(lowRankingPlayerIndex);
                    byeGiven = true;
                } else {
                    lowRankingPlayerIndex--;
                }
            }
        }

        if (!generateSubsequentRoundsPairing()) {
            badFeeling();
        }
    }
}

void gameRound::generateTopCutPairing() {
    while (playersVector.size() > 0) {
        int tableId = tablesVector.size();
        tablesVector.append(new table(playersVector.first(), playersVector.last(), tableId));
        playersVector.removeLast();
        playersVector.removeFirst();
    }
}
