#ifndef GAMEROUND_H
#define GAMEROUND_H

#include <QVector>
#include <QWidget>
#include "table.h"
#include "player.h"

/*!
 * \brief The gameRound class
 *
 * Class managing the game rounds.
 * A gameRound object creates table objects and assigns 2 players to each table according to the tournament rules
 */
class gameRound
{
public:
    /*!
     * \brief gameRound
     *
     * Class constructor
     *
     * \param tournamentPlayersVector: a vector of players to be assigned to tables
     * \param p_roundId: the related game round number
     */
    gameRound(QVector<player*> tournamentPlayersVector, int p_roundId, bool isEliminationRound, bool isRestoredFromDb = false);

    //Getters
    /*!
     * \brief getTablesVector
     * \return the tablesVector vector of table objects
     */
    QVector<table*> getTablesVector();

    /*!
     * \brief getRoundId
     * \return the roundId value
     */
    int getRoundId();

    /*!
     * \brief getIsRoundOver
     * \return the isRoundOver boolean value
     */
    bool getIsRoundOver();

    /*!
     * \brief getRoundDbId
     * \return the roundDbId value
     */
    int getRoundDbId();

    //Setters
    /*!
     * \brief setRoundDbId
     * \param dbId: a database Primary Key value
     */
    void setRoundDbId(int dbId);

    //Other methods
    /*!
     * \brief roundIsOver
     *
     * This method sets the value of isRoundOver to true
     */
    void roundIsOver();

    /*!
     * \brief generateTablesVector
     *
     * This method is called by the constructor. It creates table objects according to the number of players in playersVector.
     * For the first round, the generateFirstRoundPairings method is called
     * For subsequent rounds, the generateSubsequentRoundsPairing method is called.
     *
     * If the pairing methods return false, a dialog will inform the user of a malfunction in the program.
     *
     * If there is an odd number of players, for round 0 a randomly chosen player will receive a "bye" (not assigned to a table and awarded a score according to the tournament rules).
     * For subsequent rounds, the player with the lowest scores that haven't already received a bye will receive a bye.
     */
    void generateTablesVector();
    void addDbRestoredTable(table * restoredTable);

private:
    QVector<table*> tablesVector; /*!< A vector of table object pointers*/
    QVector<player*> playersVector; /*!< A vector of player object pointers*/
    QVector<player*> tempPlayers; /*!< A vector of player object pointers, used for pairings*/
    int roundId; /*!< The round number for a specific tournament*/
    bool isRoundOver; /*!< The status of the round*/
    int roundDbId; /*!< The Primary Key value of the round in the database*/

    /*!
     * \brief badFeeling
     *
     * This method warns the user of a problem in the program.
     */
    void badFeeling();

    /*!
     * \brief warnClubsIgnored
     *
     * This method informs the user some players from the same club had to be paired.
     */
    void warnClubsIgnored();

    /*!
     * \brief pairPlayers
     *
     * This method creates a new table pointer with two players from tempPlayers and a table number.
     * These players are then removed from tempPlayers.
     *
     * \param player0Index: Index of first player to be assigned to the table
     * \param player1Index: Index of second player to be assigned to table
     * \param tableId: Table number for display purposes
     */
    void pairPlayers(int player0Index, int player1Index, int tableId);

    /*!
     * \brief generateFirstRoundPairings
     *
     * This method randomly selects two players from tempPlayers, checks that they are from different clubs and assigns them to a table with the pairPlayers method.
     * If the disregardClubs parameter is true, players will be paired regardless of their club.
     *
     * \param disregardClubs: A boolean, used to allow pairing of players with the same club
     * \return true if all players have been paired, false otherwise
     */
    bool generateFirstRoundPairings(bool disregardClubs);

    /*!
     * \brief generateSubsequentRoundsPairing
     *
     * This method will pair players randomly within their score bracket (see Fantasy Flight Game's official X-Wing tournament rules for more details).
     *
     * \return true if all players have been paired, false otherwise
     */
    bool generateSubsequentRoundsPairing();

    /*!
     * \brief generateTopCutPairing
     *
     * This method pairs players for top cuts.
     * Unused in this version.
     */
    void generateTopCutPairing();
};

#endif // GAMEROUND_H
