#ifndef PLAYER_H
#define PLAYER_H

#include <QWidget>

/*!
 * \brief The player class
 *
 * This class manages the players assigned to a tournament.
 */
class player : public QObject
{
    Q_OBJECT

public:
    /*!
     * \brief player
     *
     * Class constructor
     * The value of tournamentPoints and marginOfVictory are set to 0
     * The value of isAssignedToTable is set to false
     *
     * \param p_name: Player name
     * \param p_factionId: Player faction, represented by a number
     * \param p_squadronPoints: Player squadron points value
     * \param p_clubDbId: Player club database Foreign Key
     */
    player(QString p_name, int p_factionId, int p_squadronPoints, bool p_hasBye, int p_clubDbId);

    //Getters
    /*!
     * \brief getName
     * \return the player's name
     */
    QString getName();

    /*!
     * \brief getFactionId
     * \return the player's faction number
     */
    int getFactionId();

    /*!
     * \brief getSquadronPoints
     * \return the player's squadron points value
     */
    int getSquadronPoints();

    /*!
     * \brief getTournamentPoints
     * \return the player's current tournament points
     */
    int getTournamentPoints();

    /*!
     * \brief getMarginOfVictory
     * \return the player's current margin of victory
     */
    int getMarginOfVictory();

    /*!
     * \brief getPlayerDbId
     * \return the database Primary Key value of the player
     */
    int getPlayerDbId();

    /*!
     * \brief getClubDbId
     * \return the database Foreign Key value of the player's club
     */
    int getClubDbId();

    /*!
     * \brief getHasBye
     * \return the hasBye member boolean value
     */
    bool getHasBye();

    /*!
     * \brief getPreviousOpponentsIdVector
     * \return the previousOpponentsIdVector
     */
    QVector<int> getPreviousOpponentsIdVector();

    /*!
     * \brief getTopCutRank
     * \return the player's topCutRank value
     */
    int getTopCutRank();

    //Setters
    void setName(QString p_name);
    void setFactionId(int p_factionId);
    void setClubDbId(int p_clubDbId);
    void setSquadronPoints(int p_squadronPoints);
    void setHasBye(bool p_hasBye);
    void setTopCutRank(int p_topCutRank);

    /*!
     * \brief setPlayerDbId
     * \param dbId: a database Primary Key value
     */
    void setPlayerDbId(int dbId);

    //Other methods
    /*!
     * \brief addScores
     *
     * This method takes two integers and adds them to the current value of tournamentPoints and marginOfVictory if they are positive.
     * It emits the updatedScores signal if successful
     *
     * \param addedTournamentPoints: an integer
     * \param addedMoV: an integer
     */
    void addScores(int addedTournamentPoints, int addedMoV);

    /*!
     * \brief addPreviousOpponentId
     *
     * This method adds a player's database Primary Key value to previousOpponentsIdVector.
     *
     * \param opponentId: A player's database Primary Key value
     */
    void addPreviousOpponentId(int opponentId);

    /*!
     * \brief receiveBye
     *
     * This method sets the hasBye boolean value to true.
     */
    void receiveBye();

    /*!
     * \brief updateDb
     *
     * This method updates the tournamentPoints and marginOfVictory values of the player record in the database
     */
    void updateDb();

    void updatePreviousScores();

signals:
    /*!
     * \brief updatedScores
     *
     * Signal emitted by the addScores method
     */
    void updatedScores();

private:
    QString name; /*!< Player's name*/
    int factionId; /*!< Player's faction id number*/
    int clubDbId; /*!< Player's club database Primary Key*/
    int squadronPoints; /*!< Player's squadron points value*/
    int tournamentPoints; /*!< Player's tournament points*/
    int marginOfVictory; /*!< Player's margin of victory*/
    int topCutRank; /*!< Player's top cut rank (if any)*/
    int previousTournamentPoints; /*!< Player's tournament points before score update, for undoing*/
    int previousMoV; /*!< Player's margin of victory before score update, for undoing*/
    int previousTopCutRank; /*!< Player's top cut rank before score update, for undoing*/
    int playerDbId; /*!< Primary Key value of the player record in the database*/
    bool hasBye; /*!< Boolean to indicate if the player has received a bye in the course of the tournament*/
    QVector<int> previousOpponentsIdVector; /*!< Vector containing the datase Primary Key values of previous opponents, to prevent multiple pairings with the same opponent during Swiss rounds*/
};

#endif // PLAYER_H
