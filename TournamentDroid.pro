#-------------------------------------------------
#
# Project created by QtCreator 2016-03-22T09:15:59
#
#-------------------------------------------------

QT       += core gui\
        sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TournamentDroid
TEMPLATE = app


SOURCES += main.cpp\
        welcomewindow.cpp \
    player.cpp \
    table.cpp \
    gameround.cpp \
    tournament.cpp \
    playerinputwindow.cpp \
    mainwindow.cpp \
    tournamentwindow.cpp \
    playerslistviewdelegate.cpp \
    playersrankingslistviewdelegate.cpp \
    tableslistviewdelegate.cpp \
    tableresultsinputdialog.cpp \
    dbmanager.cpp \
    tournamentslistviewdelegate.cpp \
    manageclubswindow.cpp

HEADERS  += welcomewindow.h \
    player.h \
    table.h \
    gameround.h \
    tournament.h \
    playerinputwindow.h \
    mainwindow.h \
    tournamentwindow.h \
    playerslistviewdelegate.h \
    playersrankingslistviewdelegate.h \
    tableslistviewdelegate.h \
    tableresultsinputdialog.h \
    dbmanager.h \
    tournamentslistviewdelegate.h \
    manageclubswindow.h

FORMS    += welcomewindow.ui \
    playerinputwindow.ui \
    mainwindow.ui \
    tournamentwindow.ui \
    tableresultsinputdialog.ui \
    manageclubswindow.ui

RESOURCES += \
    tournamentdroid.qrc

TRANSLATIONS += tournamentdroid_en.ts

RC_FILE = tournamentdroid.rc
QMAKE_LFLAGS += -static-libgcc -static-libstdc++
