#ifndef PLAYERINPUTWINDOW_H
#define PLAYERINPUTWINDOW_H

#include <QWidget>
#include <QStringList>
#include <QStringListModel>
#include <QAbstractItemView>
#include <QStandardItemModel>
#include "tournament.h"
#include "playerslistviewdelegate.h"
#include "tournamentwindow.h"
#include "dbmanager.h"

namespace Ui {
class playerInputWindow;
}

/*!
 * \brief The playerInputWindow class
 *
 * This class manages the user interface for the input of players in a tournament.
 */
class playerInputWindow : public QWidget
{
    Q_OBJECT

public:
    /*!
     * \brief playerInputWindow
     *
     * Class constructor
     *
     * \param parent
     */
    explicit playerInputWindow(QWidget *parent = 0);
    ~playerInputWindow();

public slots:
    /*!
     * \brief setCurrentTournament
     * \param p_currentTournament: tournament object
     */
    void setCurrentTournament(tournament *p_currentTournament);

    /*!
     * \brief setCurrentTournamentWindow
     * \param p_currentTournamentWindow: tournamentWindow widget
     */
    void setCurrentTournamentWindow(tournamentWindow *p_currentTournamentWindow);

    /*!
     * \brief refreshClubComboBox
     *
     * Method clearing the clubComboBox and filling it with clubs from the database.
     */
    void refreshClubComboBox();

signals:


private slots:
    /*!
     * \brief on_startTournamentPushButton_clicked
     *
     * Slot receiving the click signal from the startTournamentPushButton.
     * This method saves the tournament and included players to the database, then passes the tournament object to the tournamentWindow widget.
     */
    void on_startTournamentPushButton_clicked();

    /*!
     * \brief on_addPlayerPushButton_clicked
     *
     * Slot receiving the click signal from the addPlayerPushButton.
     * This method creates a player instance, then adds the player information to the playersListView to be displayed.
     * The input fields are cleared/reset and the focus is reset to the nameLineEdit.
     *
     * If the nameLineEdit field is left empty, the player instance will not be created and the text label next to the field will appear in red to warn the user.
     *
     */
    void on_addPlayerPushButton_clicked();

    /*!
     * \brief on_deletePlayerPushButton_clicked
     *
     * Slot receiving the click signal from the deletePlayerPushButton
     * This method will identify the selected line in the playersListView, delete that line in the listView and the appropriate player instance from memory.
     * The button is disabled unless a line is selected in the playersListView to prevent program crash.
     */
    void on_deletePlayerPushButton_clicked();

    /*!
     * \brief on_playersListView_clicked
     *
     * Slot receiving the click signal from the playersListView
     * This methods enables the deletePlayerPushButton to allow deletion of the selected player
     *
     * \param index: the selected index (unused in this method)
     */
    void on_playersListView_clicked(const QModelIndex &index);

    /*!
     * \brief on_addClubPushButton_clicked
     *
     * Slot receiving the click signal from addClubPushButton.
     * This method opens a dialog asking the user for a new club name.
     * If the user inputs text in the lineEdit and clicks OK, a new club is inserted in the database, then the refreshClubComboBox method is called.
     */
    void on_addClubPushButton_clicked();

    void drawBackgroundImg();
    void resizeEvent(QResizeEvent *evt);

private:
    Ui::playerInputWindow *ui; /*!< User interface object*/
    QStringListModel *comboBoxModel; /*!< List model to display the faction names in the comboBox*/
    tournament *currentTournament; /*!< tournament object to which the created players are added*/
    QStandardItemModel *playersListViewModel; /*!< List model to display the created players' information in the listView*/
    playersListViewDelegate *currentPlayersListViewDelegate; /*!< Delegate managing the listView line formatting*/
    tournamentWindow *currentTournamentWindow; /*!< tournamentWindow widget to which the tournament is passed on*/
};

#endif // PLAYERINPUTWINDOW_H
