#ifndef TABLERESULTSINPUTDIALOG_H
#define TABLERESULTSINPUTDIALOG_H

#include <QDialog>
#include "table.h"

namespace Ui {
class tableResultsInputDialog;
}

/*!
 * \brief The tableResultsInputDialog class
 *
 * This class managed the user interface for the input of scores for a tournament gaming table.
 * It takes a table as a parameter.
 */
class tableResultsInputDialog : public QDialog
{
    Q_OBJECT

public:
    /*!
     * \brief tableResultsInputDialog
     *
     * Class constructor
     *
     * \param p_selectedTable: a tournament gaming table
     * \param parent
     */
    explicit tableResultsInputDialog(table* p_selectedTable, int p_eliminationRoundId = -1, QWidget *parent = 0);
    ~tableResultsInputDialog();

private slots:
    /*!
     * \brief on_buttonBox_accepted
     *
     * Slot receiving the buttonBox validation signal.
     * This method will call the calculateResults method from the table object and the updateDb method for the table's players.
     * It then emits the tableUpdated signal passing on the selectedTable object.
     */
    void on_buttonBox_accepted();

    void on_player0PtsDestroyedSpinBox_valueChanged(int arg1);

    void on_player1PtsDestroyedSpinBox_valueChanged(int arg1);

private:
    Ui::tableResultsInputDialog *ui; /*!< User interface object*/
    table *selectedTable; /*!< table object*/
    int eliminationRoundId;

    void drawBackgroundImg();
    void toggleRadioButtons(bool enabledStatus);

signals:
    /*!
     * \brief tableUpdated
     *
     * Signal emitted by the on_buttonBox_accepted method.
     * This signal passes on the selectedTable object to the receiving slot.
     *
     * \param updatedTable
     */
    void tableUpdated(table * updatedTable);
};

#endif // TABLERESULTSINPUTDIALOG_H
