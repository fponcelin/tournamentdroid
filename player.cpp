#include "player.h"
#include "dbmanager.h"

player::player(QString p_name, int p_factionId, int p_squadronPoints, bool p_hasBye, int p_clubDbId)
{
    name = p_name;
    factionId = p_factionId;
    clubDbId = p_clubDbId;
    squadronPoints = p_squadronPoints;
    hasBye = p_hasBye;
    tournamentPoints = 0;
    marginOfVictory = 0;
    topCutRank = 0;
    previousTournamentPoints = 0;
    previousMoV = 0;
    previousTopCutRank = 0;
}

//Getters
QString player::getName() {
    return name;
}

int player::getFactionId() {
    return factionId;
}

int player::getSquadronPoints() {
    return squadronPoints;
}

int player::getTournamentPoints() {
    return tournamentPoints;
}

int player::getMarginOfVictory() {
    return marginOfVictory;
}

int player::getPlayerDbId() {
    return playerDbId;
}

int player::getClubDbId() {
    return clubDbId;
}

bool player::getHasBye() {
    return hasBye;
}

QVector<int> player::getPreviousOpponentsIdVector() {
    return previousOpponentsIdVector;
}

int player::getTopCutRank() {
    return topCutRank;
}

//Setters
void player::setName(QString p_name) {
    name = p_name;
}

void player::setFactionId(int p_factionId) {
    factionId = p_factionId;
}

void player::setClubDbId(int p_clubDbId) {
    clubDbId = p_clubDbId;
}

void player::setSquadronPoints(int p_squadronPoints) {
    squadronPoints = p_squadronPoints;
}

void player::setHasBye(bool p_hasBye) {
    hasBye = p_hasBye;
}

void player::setPlayerDbId(int dbId) {
    playerDbId = dbId;
}

void player::setTopCutRank(int p_topCutRank) {
    topCutRank = p_topCutRank;
    updateDb();
    emit updatedScores();
}

//Other methods
void player::addScores(int addedTournamentPoints, int addedMoV) {
    if (addedTournamentPoints >= 0 && addedMoV >= 0) {
        tournamentPoints = addedTournamentPoints + previousTournamentPoints;
        marginOfVictory = addedMoV + previousMoV;
        updateDb();
        emit updatedScores();
    }
}

void player::addPreviousOpponentId(int opponentId) {
    previousOpponentsIdVector.append(opponentId);
}

void player::receiveBye() {
    hasBye = true;
}

void player::updateDb() {
    QSqlQuery q(QSqlDatabase::database("dbConnection"));
    if (!q.prepare("UPDATE players SET playerTournamentPoints=" + QString::number(tournamentPoints) + ", playerMarginOfVictory=" + QString::number(marginOfVictory) + ", playerTopCutRank=" + QString::number(topCutRank) + " WHERE playerId=" + QString::number(playerDbId))) {
        qDebug() << q.lastError();
    }
    q.exec();
}

void player::updatePreviousScores() {
    previousTournamentPoints = tournamentPoints;
    previousMoV = marginOfVictory;
    previousTopCutRank = topCutRank;
}
