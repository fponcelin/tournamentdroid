#include "dbmanager.h"
#include <QDebug>

dbManager::dbManager(const QString &path)
{
    if (QSqlDatabase::contains("dbConnection")) {
        db = QSqlDatabase::database("dbConnection");
    } else {
        db = QSqlDatabase::addDatabase("QSQLITE", "dbConnection");
        db.setDatabaseName(path);
    }

    if (!db.open()) {
        qDebug() << "Error: connection with database failed";
    } else {
        qDebug() << "Connected to database";
    }

    QStringList tables = db.tables();
    QSqlQuery q(QSqlDatabase::database("dbConnection"));
    if (!tables.contains("appVersion", Qt::CaseInsensitive)) {
        q.exec(QLatin1String("CREATE TABLE appVersion(appVersionNb varchar primary key)"));
        q.exec(QLatin1String("INSERT INTO appVersion(appVersionNb) VALUES ('1.0')"));
    }
    if (!tables.contains("clubs", Qt::CaseInsensitive)) {
        q.exec(QLatin1String("CREATE TABLE clubs(clubId integer primary key, clubName varchar)"));
        qDebug() << q.lastError();
        QString defaultClubName = QObject::tr("Pas de club");
        q.prepare(QLatin1String("INSERT INTO clubs(clubName) VALUES (?)"));
        q.addBindValue(defaultClubName);
        q.exec();
        if (tables.contains("players", Qt::CaseInsensitive)) {
            q.exec(QLatin1String("ALTER TABLE players ADD COLUMN fk_clubId integer DEFAULT 1 REFERENCES clubs(clubId) ON DELETE SET DEFAULT"));
            q.exec(QLatin1String("ALTER TABLE players ADD COLUMN playerTopCutRank integer DEFAULT 0"));
        }
    }
    if (!tables.contains("tournaments", Qt::CaseInsensitive)) {
        q.exec(QLatin1String("CREATE TABLE tournaments(tournamentId integer primary key, tournamentName varchar, tournamentDate date, tournamentTotalRoundNb integer, tournamentHasTopCut integer, tournamentTopCutNb integer, tournamentIsOver integer DEFAULT 0)"));
    }
    if (!tables.contains("gamerounds", Qt::CaseInsensitive)) {
        q.exec(QLatin1String("CREATE TABLE gamerounds(gameRoundId integer primary key, fk_tournamentId integer REFERENCES tournaments(tournamentId) ON DELETE CASCADE, gameRoundNb integer)"));
    }
    if (!tables.contains("tables", Qt::CaseInsensitive)) {
        q.exec(QLatin1String("CREATE TABLE tables(tableId integer primary key, fk_gameRoundId integer REFERENCES gamerounds(gameRoundId) ON DELETE CASCADE, fk_tablePlayer0Id integer REFERENCES players(playerId) ON DELETE CASCADE, fk_tablePlayer1Id integer REFERENCES players(playerId) ON DELETE CASCADE, tablePlayer0Score integer, tablePlayer1Score integer, tableNb integer, tableRoundIsOver integer DEFAULT 0)"));
    }
    if (!tables.contains("players", Qt::CaseInsensitive)) {
        q.exec(QLatin1String("CREATE TABLE players(playerId integer primary key, fk_tournamentId integer REFERENCES tournaments(tournamentId) ON DELETE CASCADE, fk_clubId integer DEFAULT 1 REFERENCES clubs(clubId) ON DELETE SET DEFAULT, playerName varchar, playerFactionId integer, playerSquadronPoints integer, playerTournamentPoints integer, playerMarginOfVictory integer, playerTopCutRank integer DEFAULT 0)"));
    }
    q.exec(QLatin1String("PRAGMA foreign_keys = '1'"));
}

QVariant dbManager::addTournament(QSqlQuery &q, const QString &tournamentName, const QDate &tournamentDate, int tournamentTotalRoundNb, int tournamentHasTopCut, int tournamentTopCutNb) {
    q.addBindValue(tournamentName);
    q.addBindValue(tournamentDate);
    q.addBindValue(tournamentTotalRoundNb);
    q.addBindValue(tournamentHasTopCut);
    q.addBindValue(tournamentTopCutNb);
    q.exec();

    return q.lastInsertId();
}

int dbManager::addPlayer(QSqlQuery &q, const QVariant &fk_tournamentId, const QString &playerName, int playerFactionId, int playerSquadronPoints, int playerTournamentPoints, int playerMarginOfVictory, int playerClubDbId) {
    q.addBindValue(fk_tournamentId);
    q.addBindValue(playerName);
    q.addBindValue(playerFactionId);
    q.addBindValue(playerSquadronPoints);
    q.addBindValue(playerTournamentPoints);
    q.addBindValue(playerMarginOfVictory);
    q.addBindValue(playerClubDbId);
    q.exec();

    QVariant playerId = q.lastInsertId();
    playerId.convert(2);

    return playerId.toInt();
}

int dbManager::addGameRound(int fk_tournamentId, int gameRoundNb) {
    QSqlQuery q(QSqlDatabase::database("dbConnection"));
    if (!q.prepare(QLatin1String("INSERT INTO gamerounds(fk_tournamentId, gameRoundNb) VALUES(?, ?)"))) {
        qDebug() << q.lastError();
    }

    q.addBindValue(fk_tournamentId);
    q.addBindValue(gameRoundNb);
    q.exec();

    QVariant gameRoundId = q.lastInsertId();
    gameRoundId.convert(2);

    return gameRoundId.toInt();
}

int dbManager::addTable(table* tableToSave, int fk_gameRoundId) {
     int fk_tablePlayer0Id = tableToSave->getPlayer0()->getPlayerDbId();
     int fk_tablePlayer1Id = tableToSave->getPlayer1()->getPlayerDbId();
     int tablePlayer0Score = tableToSave->getPlayer0Score();
     int tablePlayer1Score = tableToSave->getPlayer1Score();
     int tableNb = tableToSave->getTableId();

    QSqlQuery q(QSqlDatabase::database("dbConnection"));
    if (!q.prepare(QLatin1String("INSERT INTO tables(fk_gameRoundId, fk_tablePlayer0Id, fk_tablePlayer1Id, tablePlayer0Score, tablePlayer1Score, tableNb) VALUES(?, ?, ?, ?, ?, ?)"))) {
        qDebug() << q.lastError();
        qDebug() << "Table insert failed";
    }

    q.addBindValue(fk_gameRoundId);
    q.addBindValue(fk_tablePlayer0Id);
    q.addBindValue(fk_tablePlayer1Id);
    q.addBindValue(tablePlayer0Score);
    q.addBindValue(tablePlayer1Score);
    q.addBindValue(tableNb);
    q.exec();

    QVariant tableId = q.lastInsertId();
    tableId.convert(2);

    return tableId.toInt();
}

int dbManager::saveTournamentToDb(tournament* tournamentToSave) {
    QSqlQuery q(QSqlDatabase::database("dbConnection"));
    if (!q.prepare(QLatin1String("INSERT INTO tournaments(tournamentName, tournamentDate, tournamentTotalRoundNb, tournamentHasTopCut, tournamentTopCutNb) VALUES(?, ?, ?, ?, ?)"))) {
        qDebug() << q.lastError();
    }
    QVariant tournamentId = addTournament(q, tournamentToSave->getName(), tournamentToSave->getDate(), tournamentToSave->getRoundNb(), tournamentToSave->getHasTopCut()? 1 : 0, tournamentToSave->getTopCutNb());
    qDebug() << tournamentId;

    if (!q.prepare(QLatin1String("INSERT INTO players(fk_tournamentId, playerName, playerFactionId, playerSquadronPoints, playerTournamentPoints, playerMarginOfVictory, fk_clubId) VALUES (?, ?, ?, ?, ?, ?, ?)"))) {
        qDebug() << q.lastError();
    }
    for (player *p : tournamentToSave->getPlayersVector()) {
        p->setPlayerDbId(addPlayer(q, tournamentId, p->getName(), p->getFactionId(), p->getSquadronPoints(), p->getTournamentPoints(), p->getMarginOfVictory(), p->getClubDbId()));
    }

    tournamentId.convert(2);

    return tournamentId.toInt();
}
