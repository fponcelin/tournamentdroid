#ifndef TOURNAMENTSLISTVIEWDELEGATE_H
#define TOURNAMENTSLISTVIEWDELEGATE_H

#include <QStyledItemDelegate>
#include <QLabel>
#include <QPainter>
#include <QApplication>

/*!
 * \brief The tournamentsListViewDelegate class
 *
 * This class manages the formatting of each line in the tournamentsListView
 */
class tournamentsListViewDelegate : public QStyledItemDelegate
{
public:
    enum datarole { TournamentName = Qt::UserRole + 1, TournamentDate, PlayersCount, WinnerName };

    tournamentsListViewDelegate();

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index ) const;

    static int padding;
};

#endif // TOURNAMENTSLISTVIEWDELEGATE_H
