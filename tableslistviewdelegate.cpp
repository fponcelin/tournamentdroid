#include "tableslistviewdelegate.h"

int tablesListViewDelegate::padding = 5;

tablesListViewDelegate::tablesListViewDelegate()
{

}

QSize tablesListViewDelegate::sizeHint(const QStyleOptionViewItem &  option ,
                                        const QModelIndex & index) const
{
    if(!index.isValid())
        return QSize();

    QString locale = QLocale::system().name();
    bool isLocaleFR = locale.contains("fr");

    QString tableIdText = "Table " + index.data(TableId).toString();
    QString player0NameText = index.data(Player0Name).toString();
    QString player0PtsDestroyedText;

    if (isLocaleFR)
        player0PtsDestroyedText += "Points détruits : " + index.data(Player0PtsDestroyed).toString();
    else
        player0PtsDestroyedText += "Points destroyed: " + index.data(Player0PtsDestroyed).toString();

    QString player1NameText = index.data(Player1Name).toString();

    QFont tableIdFont = QApplication::font();
    tableIdFont.setBold(true);
    tableIdFont.setPointSize(20);
    QFont player0NameFont = QApplication::font();
    player0NameFont.setBold(true);
    player0NameFont.setPointSize(16);
    QFont player1NameFont = QApplication::font();
    player1NameFont.setBold(true);
    player1NameFont.setPointSize(16);
    QFont player0PtsDestroyedFont = QApplication::font();
    QFontMetrics tableIdFm(tableIdFont);
    QFontMetrics player0NameFm(player0NameFont);
    QFontMetrics player1NameFm(player1NameFont);
    QFontMetrics player0PtsDestroyedFm(player0PtsDestroyedFont);

    /* No need for x,y here. we only need to calculate the height given the width.
     * Note that the given height is 0. That is because boundingRect() will return
     * the suitable height if the given geometry does not fit. And this is exactly
     * what we want.
     */
    QRect tableIdRect = tableIdFm.boundingRect(0, 0, option.rect.width(), 0, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, tableIdText);
    QRect player0NameRect = player0NameFm.boundingRect(0, 0, 250, 0, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, player0NameText);
    QRect player0PtsDestroyedRect = player0PtsDestroyedFm.boundingRect(0, 0, 250, 0, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, player0PtsDestroyedText);
    QRect player1NameRect = player1NameFm.boundingRect(0, 0, 250, 0, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, player1NameText);

    QSize size;
    if (player0NameRect.height() > player1NameRect.height()) {
        size = QSize(option.rect.width(), tableIdRect.height() + player0NameRect.height() + player0PtsDestroyedRect.height() +  4*padding);
    } else {
        size = QSize(option.rect.width(), tableIdRect.height() + player1NameRect.height() + player0PtsDestroyedRect.height() +  4*padding);
    }

    return size;
}

void tablesListViewDelegate::paint(QPainter *painter,
                                    const QStyleOptionViewItem &option,
                                    const QModelIndex &index) const
{
    if(!index.isValid())
        return;

    QString locale = QLocale::system().name();
    bool isLocaleFR = locale.contains("fr");

    painter->save();

    if (option.state & QStyle::State_Selected)
        painter->fillRect(option.rect, option.palette.highlight());

    QString tableIdText = tr("Table ") + index.data(TableId).toString();
    QString player0NameText = index.data(Player0Name).toString();
    QString player0PtsDestroyedText;

    if (isLocaleFR)
        player0PtsDestroyedText += "Points détruits : " + index.data(Player0PtsDestroyed).toString();
    else
        player0PtsDestroyedText += "Points destroyed: " + index.data(Player0PtsDestroyed).toString();

    QString separatorText = "VS";
    QString player1NameText = index.data(Player1Name).toString();
    QString player1PtsDestroyedText;

    if (isLocaleFR)
        player1PtsDestroyedText += "Points détruits : " + index.data(Player1PtsDestroyed).toString();
    else
        player1PtsDestroyedText += "Points destroyed: " + index.data(Player1PtsDestroyed).toString();

    QFont tableIdFont = QApplication::font();
    tableIdFont.setBold(true);
    tableIdFont.setPointSize(20);
    QFont player0NameFont = QApplication::font();
    player0NameFont.setBold(true);
    player0NameFont.setPointSize(16);
    QFont player1NameFont = QApplication::font();
    player1NameFont.setBold(true);
    player1NameFont.setPointSize(16);
    QFont player0PtsDestroyedFont = QApplication::font();
    QFont player1PtsDestroyedFont = QApplication::font();
    QFont separatorFont = QApplication::font();
    separatorFont.setPointSize(16);
    QFontMetrics tableIdFm(tableIdFont);
    QFontMetrics player0NameFm(player0NameFont);
    QFontMetrics player1NameFm(player1NameFont);
    QFontMetrics player0PtsDestroyedFm(player0PtsDestroyedFont);
    QFontMetrics player1PtsDestroyedFm(player1PtsDestroyedFont);
    QFontMetrics separatorFm(separatorFont);

    /*
     * The x,y coords are not (0,0) but values given by 'option'. So, calculate the
     * rects again given the x,y,w.
     * Note that the given height is 0. That is because boundingRect() will return
     * the suitable height if the given geometry does not fit. And this is exactly
     * what we want.
     */

    QRect tableIdRect = tableIdFm.boundingRect(option.rect.left(), option.rect.top() + padding, option.rect.width(), 0, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, tableIdText);
    QRect player0NameRect = player0NameFm.boundingRect(option.rect.left(), tableIdRect.bottom() + padding, option.rect.width()/2 - 50, 0, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, player0NameText);
    QRect player0PtsDestroyedRect = player0PtsDestroyedFm.boundingRect(option.rect.left(), player0NameRect.bottom() + padding, option.rect.width()/2 - 50, 0, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, player0PtsDestroyedText);
    QRect separatorRect = separatorFm.boundingRect(option.rect.width()/2 - 50, tableIdRect.bottom() + padding, 100, player0NameRect.height(), Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, separatorText);
    QRect player1NameRect = player1NameFm.boundingRect(option.rect.width()/2 + 50, tableIdRect.bottom() + padding, option.rect.width()/2 - 50, 0, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, player1NameText);
    QRect player1PtsDestroyedRect = player1PtsDestroyedFm.boundingRect(option.rect.width()/2 + 50, player1NameRect.bottom() + padding, option.rect.width()/2 - 50, 0, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, player1PtsDestroyedText);

    if (tableIdText.contains("OK"))
        painter->setPen(Qt::green);
    else
        painter->setPen(Qt::black);

    painter->setFont(tableIdFont);
    painter->drawText(tableIdRect, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, tableIdText);

    painter->setPen(Qt::black);

    painter->setFont(player0NameFont);
    painter->drawText(player0NameRect, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, player0NameText);

    painter->setFont(player0PtsDestroyedFont);
    painter->drawText(player0PtsDestroyedRect, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, player0PtsDestroyedText);

    painter->setFont(separatorFont);
    painter->drawText(separatorRect, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, separatorText);

    painter->setFont(player1NameFont);
    painter->drawText(player1NameRect, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, player1NameText);

    painter->setFont(player1PtsDestroyedFont);
    painter->drawText(player1PtsDestroyedRect, Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap, player1PtsDestroyedText);

    painter->restore();
}
