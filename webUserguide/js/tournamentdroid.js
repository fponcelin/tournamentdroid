function changeTask(chosenTask, buttonClicked)
{
	//Loop to remove the highlighting from any navigation button by changing its class
	for (var i=1; i<=4; i++)
	{
		document.getElementById("button" + i).className = "inactive";
	}
	document.getElementById(buttonClicked).className = "active"; //Changes the class of the current button to activate the highlighting
	document.getElementById('visibleDiv').innerHTML=document.getElementById(chosenTask).innerHTML; //Displays the appropriate HTML block
    
    $('#tutostep').html(0);
    $(".scrollback").css("visibility", "hidden");
    $(".scrollforward").css("visibility", "visible");
}

var tutoArray = [
    [
        ["This is the first window you'll see as you open the app.<br/>It's where you create new tournaments and see past tournaments you saved.", "images/Welcome_window.JPG"], 
        ["To start a new tournament, give it a name, choose a date, set the number of Swiss rounds and select the top cut bracket, if any. Then hit the \"CREATE\" button.", "images/Welcome_window-filled.JPG"],
        ["If you saved past tournaments, you can reopen them by double-clicking an item in the list. You can also select it and hit \"Open selected tournament\". If the tournament was not finished, you can resume it.</p><p>You can also delete a tournament from the list by selecting it and hitting \"Delete selected tournament\".</p><p><b>Note:</b> Tournaments are automatically saved as you run them, so you can always resume one in the event of a computer failure.", "images/Welcome_window-saved.JPG"]
    ],
    [
        ["This is where you input the participants' details.<br/><br/>Fill out their name and choose their faction from the drop-down menu.<br/><br/>If they are part of a club or a group of players, you can select it from the drop-down menu or create a new club with the \"New club\" button. Players from the same club will not be paired (as much as possible) for the first round.<br/><br/>You can also edit their squad points value (for reference purposes, this has no impact on the running of the tournament).<br/><br/>Finally, if that player has a bye, check the \"Bye\" box. That player will not be paired in the first round and will receive scoring according to tournament rules.<br/><br/>Then click the \"Add player\" button to add it to the list.", "images/Player_input_window-empty.JPG"],
        ["The players appear in the list with their faction icon, squad points, club name and if they have a bye", "images/Player_input_window-filled.JPG"],
        ["If you need to change a player's information, you can select it in the list. The fields are populated with their details. You can edit any of the fields, then hit \"Edit player\" to save.</p><p>Alternatively, you can delete a player with the \"Delete selected player\" button.</p><p>When you're ready, hit the \"Start tournament\" button to initiate the first pairing and begin your event.", "images/Player_input_window-edit.JPG"]
    ],
    [
        ["Your tournament has started!<br/><br/>This is the main window where you're going to manage your event.<br/><br/>On the left are the current rankings, with each player's tournament points and Margin of Victory. These are updated in real time as you input the scores for each table.<br/><br/>On the right are your tables, with the players' names and the table scores.", "images/Tournament_window-1.JPG"],
        ["To score a table, double click on the table or select it and hit the \"Score selected table\" button.", "images/Tournament_window-table_selected.JPG"],
        ["The scores input window opens.</p><p>Type in the points value destroyed by each player. In case of a draw, select the winning player with the appropriate radio button at the bottom.</p><p>Hit the \"OK\" button to score the table and update the players' rankings.</p><p>You can always hit \"Cancel\" to close the window without saving any values typed in.", "images/Table_scores.JPG"],
        ["After a table has been scored, its title will appear in green with the mention \"OK\". The scores are updated accordingly below and the players' rankings updated.</p><p>If you made a mistake, you can edit a table's scores any time before starting a new round.", "images/Tournament_window-2.JPG"],
        ["When editing a previously scored table, a warning appears in the scoring window to inform you. You can edit any time before starting a new round. The players' rankings will be modified accordingly.</p><p><b>Note:</b> In the current version, if you <b>resume a previously saved tournament</b> and edit a previously scored table for the current round, the table's players tournament rankings will not update properly. I'm planning to address this bug in the next update.", "images/Table_scores-edit.JPG"],
        ["Once all tables have been scored, you can click the \"NEW ROUND\" button to initiate a new round and pair the players according to their rankings.</p><p>The program will make sure no player will face the same opponent twice during Swiss rounds.</p><p>If you attempt to start a new round before scoring all tables, a pop-up warning will inform you of this grave mistake!", "images/Tournament_window-2_warning.jpeg"],
        ["As you create new rounds, you can revisit any previous round by using the \"Go to previous round\" button above the tables list", "images/Tournament_window-3.JPG"],
        ["When navigating previous rounds, the \"Score selected table\" button becomes \"Go to next round\" so you can return to the current round.</p><p>When navigating previous rounds, you cannot edit table scores.", "images/Tournament_window-4.JPG"],
        ["If you checked the Top cut option when creating your tournament, after reaching the last Swiss round, you will enter Elimination rounds.</p><p>The appropriate amount of players will be paired according to current tournament rules.", "images/Tournament_window-5.JPG"],
        ["Once you've reached and scored all tables in the final round \(Swiss round or Elimination round, depending on your tournament settings\), if you attempt to create a new round a popup will inform you your tournament has ended.</p><p>You can now announce the winner of your tournament!", "images/Tournament_window-6_warning.JPG"],
        ["You can quit your tournament at any point. Do so by either clicking the \"QUIT TOURNAMENT\" button or closing the window. A popup will ask you if you want to save your tournament. Saving it will allow you to resume the tournament if it hasn't reached its final round. You can open any saved tournament and navigate through its rounds.</p><p>If you choose not to save your tournament, all progress and information from this event will be  immediately deleted.", "images/Tournament_window-6_quit.JPG"]
    ],
    [
        ["To manage your clubs, open the File menu and click Manage clubs. The window below opens.<br/><br/>You can see all the clubs you've created. You can create new clubs and edit or delete the ones you've created.", "images/Manage_clubs_window.JPG"],
        ["To edit or delete a club, select it in the list. Its name appears in the field under the list.</p><p>You can change the name and save it by clicking the \"Edit\" button.</p><p>To delete the club, select it and click the \"Delete selected club\" button."]
    ]
];

function scrollTuto(tutoId, scrollDirection)
{
    
    
    var currentStep = Number($('#tutostep').html());
    var imgEl = $('#tutorialimg'+tutoId);
    var txtEl = $('#tutorialtxt'+tutoId);

    if (scrollDirection=='forward') {

        imgEl.animate({left: '-150%'});
        imgEl.fadeOut(0, function() {
            imgEl.animate({left: "150%"}, 0);
            imgEl.attr("src", tutoArray[tutoId][currentStep+1][1]);
            imgEl.fadeIn();
            imgEl.animate({left: '0%'});
        });
        
        txtEl.fadeOut("slow", function() {
            txtEl.html(tutoArray[tutoId][currentStep+1][0]);
            txtEl.fadeIn("slow");
        });
        
        $('#tutostep').html(currentStep+1);
        if (Number($('#tutostep').html()) > 0) {
            $(".scrollback").css("visibility", "visible");
            
            if (Number($('#tutostep').html()) == tutoArray[tutoId].length - 1) {
                $(".scrollforward").css("visibility", "hidden");
            }
        }
    } else {
        imgEl.animate({left: '150%'});
        imgEl.fadeOut(0, function() {
            imgEl.animate({left: "-150%"}, 0);
            imgEl.attr("src", tutoArray[tutoId][currentStep-1][1]);
            imgEl.fadeIn();
            imgEl.animate({left: '0%'});
        });

        txtEl.fadeOut("slow", function() {
            txtEl.html(tutoArray[tutoId][currentStep-1][0]);
            txtEl.fadeIn("slow");
        });
        
        $('#tutostep').html(currentStep-1);
        if (Number($('#tutostep').html()) == 0) {
            $(".scrollback").css("visibility", "hidden");
        }
        if (Number($('#tutostep').html()) < tutoArray[tutoId].length - 1) {
                $(".scrollforward").css("visibility", "visible");
            }
    }      
}