<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <source>TournamentDroid</source>
        <translation>TournamentDroid</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="55"/>
        <source>Fichier</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="62"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <source>Quitter</source>
        <translation>Quit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="77"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="85"/>
        <source>Guide d&apos;utilisation</source>
        <translation>User guide</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="93"/>
        <source>Modifier les clubs...</source>
        <translation>Manage clubs...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>A propos de TournamentDroid...</source>
        <translation>About TournamentDroid...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="92"/>
        <source>A propos de TournamentDroid</source>
        <translation>About TournamentDroid</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="dbmanager.cpp" line="28"/>
        <source>Pas de club</source>
        <translation>No club</translation>
    </message>
    <message>
        <location filename="gameround.cpp" line="63"/>
        <source>Des joueurs du même club ont été placés ensemble.
Je suis désolé mais je ne pouvais pas faire autrement !</source>
        <translation>Players from the same club have been paired together.
I&apos;m sorry but I had no other choice!</translation>
    </message>
</context>
<context>
    <name>dbManager</name>
    <message>
        <source>Pas de club</source>
        <translation type="vanished">No club</translation>
    </message>
</context>
<context>
    <name>gameRound</name>
    <message>
        <source>Des joueurs du même club ont été placés ensemble.
Je suis désolé mais je ne pouvais pas faire autrement !</source>
        <translation type="vanished">Players from the same club have been paired together.
I&apos;m sorry but I had no other choice!</translation>
    </message>
</context>
<context>
    <name>manageClubsWindow</name>
    <message>
        <location filename="manageclubswindow.ui" line="99"/>
        <location filename="manageclubswindow.ui" line="116"/>
        <source>Gestion des clubs</source>
        <translation>Manage clubs</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="144"/>
        <source>Fermer la fenêtre</source>
        <translation>Close window</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="147"/>
        <source>Fermer</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="160"/>
        <source>Entrer le nom d&apos;un nouveau club</source>
        <translation>Enter new club name</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="185"/>
        <source>Ajouter le club</source>
        <translation>Add club</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="188"/>
        <source>Ajouter</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="201"/>
        <source>Nouveau club :</source>
        <translation>New club:</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="229"/>
        <source>Valider le nom modifié</source>
        <translation>Save edited name</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="232"/>
        <source>Modifier</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="245"/>
        <source>Modifier le nom du club sélectionner</source>
        <translation>Edit selected club name</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="258"/>
        <source>Nom du club sélectionné :</source>
        <translation>Selected club name:</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="289"/>
        <source>Supprimer le club sélectionné</source>
        <translation>Delete selected club</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="292"/>
        <source>Supprimer le club sélectionné de la liste</source>
        <translation>Delete selected club from the list</translation>
    </message>
    <message>
        <location filename="manageclubswindow.ui" line="318"/>
        <source>Mes clubs :</source>
        <translation>My clubs:</translation>
    </message>
</context>
<context>
    <name>playerInputWindow</name>
    <message>
        <location filename="playerinputwindow.ui" line="99"/>
        <source>Nouveau tournoi</source>
        <translation>New tournament</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="130"/>
        <source>Ajout des joueurs au tournoi</source>
        <translation>Add tournament players</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="168"/>
        <source>Nom :</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="190"/>
        <source>Faction :</source>
        <translation>Faction:</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="212"/>
        <source>Club / Groupe :</source>
        <translation>Club / Group:</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="234"/>
        <source>Points de l&apos;escadron :</source>
        <translation>Squad points:</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="256"/>
        <source>Bye :</source>
        <translation>Bye:</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="282"/>
        <source>Entrer le nom du joueur</source>
        <translation>Enter player&apos;s name</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="295"/>
        <source>Choisir la faction du joueur</source>
        <translation>Select player&apos;s faction</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="322"/>
        <source>Choisir un club pour le joueur (facultatif)</source>
        <translation>Select player&apos;s club (optional)</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="344"/>
        <source>Créer un nouveau club</source>
        <translation>Create new club</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="347"/>
        <location filename="playerinputwindow.cpp" line="197"/>
        <source>Nouveau club</source>
        <translation>New club</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="367"/>
        <source>Entrer la valeur de l&apos;escadron du joueur</source>
        <translation>Enter player&apos;s squad points value</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="404"/>
        <source>Cocher si le joueur a un bye pour le premier tour</source>
        <translation>Check if player has first round bye</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="432"/>
        <source>Enregistrer le joueur</source>
        <translation>Save player</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="435"/>
        <source>Ajouter joueur</source>
        <translation>Add player</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="464"/>
        <source>Joueurs ajoutés</source>
        <translation>Added players</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="489"/>
        <source>Supprimer le joueur sélectionné</source>
        <translation>Delete selected player</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="492"/>
        <source>Supprimer joueur sélectionné</source>
        <translation>Delete selected player</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="530"/>
        <source>Terminer l&apos;ajout de joueurs et commencer le tournoi</source>
        <translation>Finish adding players and start tournament</translation>
    </message>
    <message>
        <location filename="playerinputwindow.ui" line="533"/>
        <source>Commencer tournoi</source>
        <translation>Start tournament</translation>
    </message>
    <message>
        <location filename="playerinputwindow.cpp" line="26"/>
        <source>Rebelles</source>
        <translation>Rebels</translation>
    </message>
    <message>
        <location filename="playerinputwindow.cpp" line="26"/>
        <source>Empire</source>
        <translation>Empire</translation>
    </message>
    <message>
        <location filename="playerinputwindow.cpp" line="26"/>
        <source>Racailles &amp; Scélérats</source>
        <translation>Scum &amp; Villainy</translation>
    </message>
    <message>
        <location filename="playerinputwindow.cpp" line="198"/>
        <source>Nom du club :</source>
        <translation>Club name:</translation>
    </message>
</context>
<context>
    <name>playersListViewDelegate</name>
    <message>
        <source> points</source>
        <translation type="vanished"> points</translation>
    </message>
</context>
<context>
    <name>playersRankingsListViewDelegate</name>
    <message>
        <source>Marge de Victoire : </source>
        <translation type="vanished">Margin of Victory: </translation>
    </message>
    <message>
        <source> Pts</source>
        <translation type="vanished"> Pts</translation>
    </message>
</context>
<context>
    <name>tableResultsInputDialog</name>
    <message>
        <source>Finaliser table</source>
        <translation type="vanished">Table scores</translation>
    </message>
    <message>
        <location filename="tableresultsinputdialog.ui" line="99"/>
        <source>Résultats de la table</source>
        <translation>Table scores</translation>
    </message>
    <message>
        <location filename="tableresultsinputdialog.ui" line="128"/>
        <source>Résultats de la table </source>
        <translation>Table scores</translation>
    </message>
    <message>
        <location filename="tableresultsinputdialog.ui" line="560"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="tableresultsinputdialog.ui" line="592"/>
        <source>Joueur 1 : </source>
        <translation>Player 1:</translation>
    </message>
    <message>
        <location filename="tableresultsinputdialog.ui" line="609"/>
        <location filename="tableresultsinputdialog.ui" line="826"/>
        <source>player0Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="tableresultsinputdialog.ui" line="633"/>
        <location filename="tableresultsinputdialog.ui" line="737"/>
        <source>Points détruits : </source>
        <translation>Points destroyed:</translation>
    </message>
    <message>
        <location filename="tableresultsinputdialog.ui" line="649"/>
        <source>Points d&apos;escadrons détruits par le joueur 1</source>
        <translation>Squad points destroyed by player 1</translation>
    </message>
    <message>
        <location filename="tableresultsinputdialog.ui" line="696"/>
        <source>Joueur 2 : </source>
        <translation>Player 2:</translation>
    </message>
    <message>
        <location filename="tableresultsinputdialog.ui" line="713"/>
        <location filename="tableresultsinputdialog.ui" line="836"/>
        <source>player1Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="tableresultsinputdialog.ui" line="753"/>
        <source>Points d&apos;escadrons détruits par le joueur 2</source>
        <translation>Squad points destroyed by player 2</translation>
    </message>
    <message>
        <location filename="tableresultsinputdialog.ui" line="798"/>
        <source>En cas d&apos;égalité : choix du vainqueur</source>
        <oldsource>En cas d&apos;egalite : choix du vainqueur</oldsource>
        <translation>In case of draw: select winner</translation>
    </message>
    <message>
        <location filename="tableresultsinputdialog.cpp" line="23"/>
        <source>Cette table a déjà reçu ses scores
Vous pouvez les modifier</source>
        <translation>This table has already been scored
You may edit the scores</translation>
    </message>
</context>
<context>
    <name>tablesListViewDelegate</name>
    <message>
        <source>Points détruits : </source>
        <translation type="vanished">Points destroyed: </translation>
    </message>
    <message>
        <location filename="tableslistviewdelegate.cpp" line="80"/>
        <source>Table </source>
        <translation>Table </translation>
    </message>
</context>
<context>
    <name>tournamentWindow</name>
    <message>
        <location filename="tournamentwindow.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="tournamentwindow.ui" line="96"/>
        <source>TournamentName</source>
        <translation></translation>
    </message>
    <message>
        <location filename="tournamentwindow.ui" line="141"/>
        <source>Classement des joueurs</source>
        <translation>Players rankings</translation>
    </message>
    <message>
        <location filename="tournamentwindow.ui" line="196"/>
        <source>Quitter le tournoi et revenir à l&apos;accueil</source>
        <oldsource>Terminer le tournoi et revenir à l&apos;accueil</oldsource>
        <translation>Quit tournament and return to home screen</translation>
    </message>
    <message>
        <location filename="tournamentwindow.ui" line="199"/>
        <source>Quitter tournoi</source>
        <oldsource>Terminer tournoi</oldsource>
        <translation>Quit tournament</translation>
    </message>
    <message>
        <location filename="tournamentwindow.ui" line="242"/>
        <source>Voir les tables du tour précédent</source>
        <translation>Go to previous round</translation>
    </message>
    <message>
        <location filename="tournamentwindow.ui" line="245"/>
        <source>&lt;       Voir tour précédent</source>
        <translation>&lt;       Go to previous round</translation>
    </message>
    <message>
        <location filename="tournamentwindow.ui" line="259"/>
        <source>gameRoundNb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="tournamentwindow.ui" line="284"/>
        <source>Terminer table selectionnée</source>
        <translation>Score selected table</translation>
    </message>
    <message>
        <location filename="tournamentwindow.ui" line="354"/>
        <source>Commencer un nouveau tour</source>
        <translation>Start a new round</translation>
    </message>
    <message>
        <location filename="tournamentwindow.ui" line="357"/>
        <source>Nouveau tour</source>
        <translation>New round</translation>
    </message>
    <message>
        <location filename="tournamentwindow.cpp" line="91"/>
        <source>Vous allez quitter ce tournoi.</source>
        <translation>You are about to quit this tournament.</translation>
    </message>
    <message>
        <location filename="tournamentwindow.cpp" line="92"/>
        <source>Voulez-vous l&apos;enregistrer ?</source>
        <translation>Do you want to save it?</translation>
    </message>
    <message>
        <location filename="tournamentwindow.cpp" line="95"/>
        <source>Enregistrer</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="tournamentwindow.cpp" line="184"/>
        <source>Vainqueur</source>
        <translation>Winner</translation>
    </message>
    <message>
        <location filename="tournamentwindow.cpp" line="195"/>
        <source>Ronde suisse Nº</source>
        <translation>Swiss round Nº</translation>
    </message>
    <message>
        <location filename="tournamentwindow.cpp" line="228"/>
        <source>La ronde en cours n&apos;est pas terminée.
Veuillez entrer les résultats pour toutes les tables.</source>
        <translation>The current round is not over.
Please score all tables.</translation>
    </message>
    <message>
        <location filename="tournamentwindow.cpp" line="235"/>
        <source>Le tournoi a atteint son dernier tour, vous ne pouvez pas commencer de nouvelle ronde.</source>
        <translation>The tournament reached its final round, you can&apos;t start a new one.</translation>
    </message>
</context>
<context>
    <name>tournamentsListViewDelegate</name>
    <message>
        <source> participants</source>
        <translation type="vanished"> participants</translation>
    </message>
    <message>
        <source>Vainqueur : </source>
        <translation type="vanished">Winner: </translation>
    </message>
</context>
<context>
    <name>welcomeWindow</name>
    <message>
        <location filename="welcomewindow.ui" line="20"/>
        <source>TournamentDroid</source>
        <oldsource>Tournament Droid</oldsource>
        <translation>TournamentDroid</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="165"/>
        <source>Nouveau tournoi</source>
        <translation>New tournament</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="186"/>
        <source>Date :</source>
        <translation>Date:</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="202"/>
        <source>Date du tournoi</source>
        <translation>Tournament date</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="223"/>
        <source>Nom du tournoi :</source>
        <translation>Tournament name:</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="239"/>
        <source>Nom du tournoi</source>
        <translation>Tournament name</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="257"/>
        <source>Rondes suisses :</source>
        <translation>Swiss rounds:</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="273"/>
        <source>Nombre de rondes Suisses</source>
        <translation>Number of Swiss rounds</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="304"/>
        <source>Créer nouveau tournoi</source>
        <translation>Create new tournament</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="307"/>
        <source>Créer</source>
        <translation>Create</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="331"/>
        <source>Top cut :</source>
        <translation>Top cut:</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="347"/>
        <source>Cocher si le tournoi a un top cut</source>
        <translation>Check if tournament has a top cut</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="366"/>
        <source>Choisir le nombre de joueurs pour les tours éliminatoires</source>
        <translation>Select number of players for single elimination rounds</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="462"/>
        <source>Mes tournois</source>
        <translation>My tournaments</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="587"/>
        <source>Supprimer le tournoi sélectionné</source>
        <translation>Delete selected tournament</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="590"/>
        <source>Supprimer tournoi sélectionné</source>
        <translation>Delete selected tournament</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="628"/>
        <source>Ouvrir le tournoi sélectionné</source>
        <translation>Open selected tournament</translation>
    </message>
    <message>
        <location filename="welcomewindow.ui" line="631"/>
        <source>Ouvrir tournoi sélectionné</source>
        <translation>Open selected tournament</translation>
    </message>
    <message>
        <source>Ouvrir tournoi selectionne</source>
        <translation type="vanished">Open selected tournament</translation>
    </message>
    <message>
        <location filename="welcomewindow.cpp" line="106"/>
        <source>Vous allez supprimer ce tournoi.</source>
        <translation>This tournament will be deleted.</translation>
    </message>
    <message>
        <location filename="welcomewindow.cpp" line="107"/>
        <source>Etes-vous sûr ?</source>
        <translation>Are you sure?</translation>
    </message>
</context>
</TS>
