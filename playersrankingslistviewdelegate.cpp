#include "playersrankingslistviewdelegate.h"

QSize playersRankingsListViewDelegate::iconSize = QSize(30, 30);
int playersRankingsListViewDelegate::padding = 5;

playersRankingsListViewDelegate::playersRankingsListViewDelegate()
{

}

QSize playersRankingsListViewDelegate::sizeHint(const QStyleOptionViewItem &  option ,
                                        const QModelIndex & index) const
{
    if(!index.isValid())
        return QSize();

    QString locale = QLocale::system().name();
    bool isLocaleFR = locale.contains("fr");

    QString playerNameText = index.data(PlayerName).toString();
    QString playerMoVText;

    if (isLocaleFR)
        playerMoVText = "Marge de Victoire : " + index.data(PlayerMoV).toString();
    else
        playerMoVText = "Margin of Victory: " + index.data(PlayerMoV).toString();

    QFont playerNameFont = QApplication::font();
    playerNameFont.setBold(true);
    playerNameFont.setPointSize(16);
    QFont playerTournamentPtsFont = QApplication::font();
    playerTournamentPtsFont.setBold(true);
    playerTournamentPtsFont.setPointSize(16);
    QFont playerMovFont = QApplication::font();
    QFontMetrics playerNameFm(playerNameFont);
    QFontMetrics playerMovFm(playerMovFont);

    /* No need for x,y here. we only need to calculate the height given the width.
     * Note that the given height is 0. That is because boundingRect() will return
     * the suitable height if the given geometry does not fit. And this is exactly
     * what we want.
     */
    QRect playerNameRect = playerNameFm.boundingRect(0, 0,
                                                        option.rect.width() - (iconSize.width() + 30 + 100 + padding), 0,
                                                        Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                                        playerNameText);
    QRect playerMoVRect = playerMovFm.boundingRect(0, 0,
                                                   option.rect.width() - iconSize.width(), 0,
                                                   Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                                   playerMoVText);

    QSize size(option.rect.width(), playerNameRect.height() + playerMoVRect.height() +  3*padding);

    /* Keep the minimum height needed in mind. */
    if(size.height()<iconSize.height())
        size.setHeight(iconSize.height());

    return size;
}

void playersRankingsListViewDelegate::paint(QPainter *painter,
                                    const QStyleOptionViewItem &option,
                                    const QModelIndex &index) const
{
    if(!index.isValid())
        return;

    QString locale = QLocale::system().name();
    bool isLocaleFR = locale.contains("fr");

    painter->save();

    if (option.state & QStyle::State_Selected)
        painter->fillRect(option.rect, option.palette.highlight());

    QString playerIndexText = index.data(PlayerIndex).toString();
    QString playerNameText = index.data(PlayerName).toString();
    QString playerTournamentPtsText = index.data(PlayerTournamentPts).toString() + " Pts";
    QString playerMoVText;

    if (isLocaleFR)
        playerMoVText = "Marge de Victoire : " + index.data(PlayerMoV).toString();
    else
        playerMoVText = "Margin of Victory: " + index.data(PlayerMoV).toString();

    QString playerTopCutBracketText = index.data(TopCutBracket).toString();

    QFont playerIndexFont = QApplication::font();
    playerIndexFont.setBold(true);
    playerIndexFont.setPointSize(16);
    QFont playerNameFont = QApplication::font();
    playerNameFont.setBold(true);
    playerNameFont.setPointSize(16);
    QFont playerTournamentPtsFont = QApplication::font();
    playerTournamentPtsFont.setBold(true);
    playerTournamentPtsFont.setPointSize(16);
    QFont playerMovFont = QApplication::font();
    QFont playerTopCutBracketFont = QApplication::font();
    QFontMetrics playerIndexFm(playerIndexFont);
    QFontMetrics playerNameFm(playerNameFont);
    QFontMetrics playerTournamentPtsFm(playerTournamentPtsFont);
    QFontMetrics playerMovFm(playerMovFont);
    QFontMetrics playerTopCutBracketFm(playerTopCutBracketFont);

    /*
     * The x,y coords are not (0,0) but values given by 'option'. So, calculate the
     * rects again given the x,y,w.
     * Note that the given height is 0. That is because boundingRect() will return
     * the suitable height if the given geometry does not fit. And this is exactly
     * what we want.
     */
    QRect playerIndexRect = QRect(option.rect.left(), option.rect.top() + option.rect.height()/2 - 17,30, iconSize.height());
    QRect playerNameRect = playerNameFm.boundingRect(option.rect.left() + iconSize.width() + padding + 30, option.rect.top() + padding,
                                                  option.rect.width() - (iconSize.width() + 30 + 100 + padding), 0,
                                                  Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                                  playerNameText);
    QRect playerTournamentPtsRect = playerTournamentPtsFm.boundingRect(option.rect.right() - padding - 100, option.rect.top() + padding,
                                                                         100, 0,
                                                                         Qt::AlignRight|Qt::AlignTop|Qt::TextWordWrap,
                                                                         playerTournamentPtsText);
    QRect playerMoVRect = playerMovFm.boundingRect(playerNameRect.left() + 10, playerNameRect.bottom() + padding,
                                                   option.rect.width() - iconSize.width(), 0,
                                                   Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                                   playerMoVText);
    QRect playerTopCutBracketRect = playerTopCutBracketFm.boundingRect(playerMoVRect.right() + 10, playerNameRect.bottom() + padding,
                                                                       option.rect.width() - iconSize.width() - playerMoVRect.width(), 0,
                                                                       Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                                                       playerTopCutBracketText);

    QImage icon = QImage(":/img/" + index.data(PlayerFaction).toString() + ".png");
    QRect imageRect = QRect(option.rect.left() + 30, option.rect.top() + option.rect.height()/2 - 15, icon.width(), iconSize.height());
    painter->drawImage(imageRect, icon);

    painter->setPen(Qt::black);

    painter->setFont(playerIndexFont);
    painter->drawText(playerIndexRect, Qt::AlignCenter, playerIndexText);

    painter->setFont(playerNameFont);
    painter->drawText(playerNameRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, playerNameText);

    painter->setFont(playerTournamentPtsFont);
    painter->drawText(playerTournamentPtsRect, Qt::AlignRight|Qt::AlignTop|Qt::TextWordWrap, playerTournamentPtsText);

    painter->setFont(playerMovFont);
    painter->drawText(playerMoVRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, playerMoVText);

    painter->setFont(playerTopCutBracketFont);
    painter->drawText(playerTopCutBracketRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, playerTopCutBracketText);

    painter->restore();
}
