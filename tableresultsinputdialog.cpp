#include "tableresultsinputdialog.h"
#include "ui_tableresultsinputdialog.h"
#include <QPalette>
#include <QPainter>

tableResultsInputDialog::tableResultsInputDialog(table* p_selectedTable, int p_eliminationRoundId, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::tableResultsInputDialog)
{
    ui->setupUi(this);
    drawBackgroundImg();
    selectedTable = p_selectedTable;
    eliminationRoundId = p_eliminationRoundId;

    ui->player0NameLabel->setText(selectedTable->getPlayer0()->getName());
    ui->player1NameLabel->setText(selectedTable->getPlayer1()->getName());
    ui->player0RadioButton->setText(selectedTable->getPlayer0()->getName());
    ui->player1RadioButton->setText(selectedTable->getPlayer1()->getName());

    if (!selectedTable->getIsTableRoundOver()) {
        ui->TableIsFinishedWarninglabel->clear();
    } else {
        ui->TableIsFinishedWarninglabel->setText(tr("Cette table a déjà reçu ses scores\nVous pouvez les modifier"));
        ui->player0PtsDestroyedSpinBox->setValue(selectedTable->getPlayer0Score());
        ui->player1PtsDestroyedSpinBox->setValue(selectedTable->getPlayer1Score());
    }
}

tableResultsInputDialog::~tableResultsInputDialog()
{
    delete ui;
}

void tableResultsInputDialog::on_buttonBox_accepted()
{
    int winnerIndex = -1;
    if (ui->player0RadioButton->isChecked())
        winnerIndex = 0;
    if (ui->player1RadioButton->isChecked())
        winnerIndex = 1;
    if (eliminationRoundId < 0) {
        selectedTable->calculateTableRoundResult(ui->player0PtsDestroyedSpinBox->value(), ui->player1PtsDestroyedSpinBox->value(), winnerIndex);
    } else {
        selectedTable->calculateTableEliminationRoundResult(ui->player0PtsDestroyedSpinBox->value(), ui->player1PtsDestroyedSpinBox->value(), eliminationRoundId);
    }
    selectedTable->getPlayer0()->updateDb();
    selectedTable->getPlayer1()->updateDb();
    selectedTable->updateDb();
    emit tableUpdated(selectedTable);
}

void tableResultsInputDialog::drawBackgroundImg() {
    QImage BgTile(":/img/Bg_TableResultsInput_Fill.png");
    QImage BgTopImg(":/img/Bg_TableResultsInput_Top.png");
    QImage BgBottomImg(":/img/Bg_TableResultsInput_Bottom.png");

    QPixmap bgImg(this->size());
    QPainter paint(&bgImg);
    for (int i=0; i<this->height(); i+=50) {
        paint.drawImage(this->rect().left(),i,BgTile);
    }
    paint.drawImage(this->rect().left(),this->rect().top(),BgTopImg);
    paint.drawImage(this->rect().left(),this->rect().bottom() - 49,BgBottomImg);

    QPalette palette = this->palette();
    palette.setBrush(QPalette::Background, bgImg);//set the pic to the background
    this->setPalette(palette); //show the background pic
}

void tableResultsInputDialog::toggleRadioButtons(bool enabledStatus) {
    ui->winnerChoiceLabel->setEnabled(enabledStatus);
    ui->player0RadioButton->setEnabled(enabledStatus);
    ui->player1RadioButton->setEnabled(enabledStatus);
}

void tableResultsInputDialog::on_player0PtsDestroyedSpinBox_valueChanged(int arg1)
{
    if (arg1 == ui->player1PtsDestroyedSpinBox->value()) {
        toggleRadioButtons(true);
    } else {
        toggleRadioButtons(false);
    }
}

void tableResultsInputDialog::on_player1PtsDestroyedSpinBox_valueChanged(int arg1)
{
    if (arg1 == ui->player0PtsDestroyedSpinBox->value()) {
        toggleRadioButtons(true);
    } else {
        toggleRadioButtons(false);
    }
}
