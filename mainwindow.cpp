#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QCloseEvent>
#include <QDebug>
#include <QDesktopServices>
#include <QCloseEvent>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    welcomew = new welcomeWindow(this);
    tournamentw = new tournamentWindow(this);

    mainWidget = new QStackedWidget;
    mainWidget->addWidget(welcomew);
    mainWidget->addWidget(tournamentw);

    connect(tournamentw, SIGNAL(receivedTournament()), this, SLOT(tournamentStarts()));
    connect(tournamentw, SIGNAL(tournamentEnded()), this, SLOT(tournamentEnds()));
    welcomew->setCurrentTournamentWindow(tournamentw);

    mainWidget->setCurrentIndex(0);
    setCentralWidget(mainWidget);

    connect(ui->actionQuit, SIGNAL(triggered(bool)), this, SLOT(quitApp()));
    connect(ui->actionUserGuide, SIGNAL(triggered(bool)), this, SLOT(openHelpFile()));
    connect(ui->actionManageClubs, SIGNAL(triggered(bool)), this, SLOT(openManageClubs()));
    connect(ui->actionAbout, SIGNAL(triggered(bool)), this, SLOT(openAbout()));

    drawBackgroundImg();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete welcomew;
    delete tournamentw;
}

void MainWindow::closeEvent(QCloseEvent *event) {
    if (mainWidget->currentIndex() == 1) {
        tournamentw->on_finishTournamentPushButton_clicked();
    }
    if (mainWidget->currentIndex() == 0) {
        event->accept();
    } else {
        event->ignore();
    }
}

void MainWindow::resizeEvent(QResizeEvent *evt) {
    drawBackgroundImg();
    QMainWindow::resizeEvent(evt);
}

void MainWindow::tournamentStarts() {
    mainWidget->setCurrentIndex(1);
}

void MainWindow::tournamentEnds() {
    qDebug() << "received tournament end signal";
    mainWidget->setCurrentIndex(0);
    welcomew->refreshTournamentsListView();
}

void MainWindow::openHelpFile() {
    qDebug() << "HELP!!!";
    QSettings settings("Florian Poncelin", "TournamentDroid");
    if (!QDesktopServices::openUrl(QUrl::fromLocalFile(settings.value("webUserGuidePath").toString()))) {
        QDesktopServices::openUrl(QUrl::fromLocalFile(settings.value("webUserGuidePathWin10").toString()));
    }
}

void MainWindow::quitApp() {
    if (mainWidget->currentIndex() == 1) {
        tournamentw->on_finishTournamentPushButton_clicked();
    }
    if (mainWidget->currentIndex() == 0) {
        QWidget::close();
    }
}

void MainWindow::openManageClubs() {
    manageClubsw = new manageClubsWindow();
    manageClubsw->show();
}

void MainWindow::openAbout() {
    QString aboutTitle = tr("A propos de TournamentDroid");
    QString aboutText = "<p>Copyright 2016 Florian Poncelin</p><p>TournamentDroid is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.</p><p>TournamentDroid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser GNU Lesser General Public License for more details.</p><p>You should have received a copy of the GNU Lesser General Public License along with TournamentDroid. If not, see <a href='http://www.gnu.org/licenses/'>http://www.gnu.org/licenses/</a>.</p><p>The full source code of this program is available at <a href='https://bitbucket.org/fponcelin/tournamentdroid/'>https://bitbucket.org/fponcelin/tournamentdroid/</a>.</p>";

    QMessageBox about(this);
    about.setWindowTitle(aboutTitle);
    about.setTextFormat(Qt::RichText);
    about.setText(aboutText);
    about.exec();
}

void MainWindow::drawBackgroundImg() {
    //if (mainWidget->currentIndex() == 1) {
        //QImage BgTile(":/img/Bg_Tile.jpg");
        QImage BgFill(":/img/Bg_Fill.png");
        QImage BgTopLeftImg(":/img/Bg_Top_Left.png");
        QImage BgTopRightImg(":/img/Bg_Top_Right.png");
        QImage BgTopCenterImg(":/img/Bg_Top_Center.png");
        QImage BgTopCenterLCDImg(":/img/Bg_Top_Center_LCD.png");
        QImage BgBottomLeftImg(":/img/Bg_Bottom_Left.png");
        QImage BgBottomRightImg(":/img/Bg_Bottom_Right.png");
        QImage BgBottomCenterImg(":/img/Bg_Bottom_Center.png");
        QImage BgBottomCenterLCDImg(":/img/Bg_Bottom_Center_LCD.png");

        int xBgLeft;
        int xBgRight;
        int bgFillWidth;
        if (this->width() > 1340) {
            xBgLeft = this->rect().center().x() - 670;
            xBgRight = this->rect().center().x() + 370;
            bgFillWidth = 1340;
        } else {
            xBgLeft = this->rect().left();
            xBgRight = this->rect().right() - 299;
            bgFillWidth = this->width();
        }

        QPixmap bgImg(this->size());
        QPainter paint(&bgImg);
        /*for (int i=0; i<this->height(); i+=300) {
            for (int j=0; j<this->width(); j+=300) {
                paint.drawImage(j,i,BgTile);
            }
        }*/
        paint.drawImage(QRect(xBgLeft, this->rect().top(), bgFillWidth,this->rect().height()),BgFill);
        paint.drawImage(xBgLeft,this->rect().top(),BgTopLeftImg);
        paint.drawImage(QRect(xBgLeft+300, this->rect().top(), xBgRight - (xBgLeft + 300),300),BgTopCenterImg);
        paint.drawImage(xBgRight,this->rect().top(),BgTopRightImg);
        paint.drawImage(this->rect().center().x()-150, this->rect().top(), BgTopCenterLCDImg);
        paint.drawImage(xBgLeft,this->rect().bottom() - 99,BgBottomLeftImg);
        paint.drawImage(QRect(xBgLeft+300, this->rect().bottom()-99, xBgRight - (xBgLeft + 300),100),BgBottomCenterImg);
        paint.drawImage(xBgRight,this->rect().bottom() - 99,BgBottomRightImg);
        paint.drawImage(this->rect().center().x()-150, this->rect().bottom()-99, BgBottomCenterLCDImg);
        //bgImg.scaled(this->size(), Qt::IgnoreAspectRatio); //set scale of pic to match the window

        QPalette palette = this->palette();
        palette.setBrush(QPalette::Background, bgImg);//set the pic to the background
        this->setPalette(palette); //show the background pic
    //}
}
